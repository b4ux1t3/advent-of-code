module DayTwo =
    open System
    open System.Text.RegularExpressions 
    open AdventOfCode.Utilities
    type Vector3 = int * int * int
    let makeVector3 x y z = Vector3 (x,y,z)
    let addVector3 vec1 vec2 =
        match (vec1, vec2) with
        | (x1,y1,z1), (x2,y2,z2)  -> Vector3 (x1+x2, y1+y2, z1+z2)
    let lines = InputGetter.GetInputs(Environment.GetCommandLineArgs(), 1)
    let commandRegex = "(?<DIRECTION>forward|up|down) (?<MAGNITUDE>\d+)"
    let parseCommandToVector3 command =
        let groups = Regex.Match(command, commandRegex).Groups
        let magnitude = int groups["MAGNITUDE"].Captures[0].Value
        match groups["DIRECTION"].Captures[0].Value with
        | "forward" -> makeVector3 magnitude 0 0
        | "up" -> makeVector3 0 -magnitude 0  // Going up decreases our depth. . .
        | "down" -> makeVector3 0 magnitude 0 // and going down increases our depth.
        | _ -> makeVector3 0 0 0
    
    module PartOne =
        let movements =
            lines |> Seq.map parseCommandToVector3
        let finalPosition =
            movements |> Seq.reduce addVector3
        let answer =
            match finalPosition with
            | x,y,_ -> x * y
        
        printfn $"PART ONE\nFinal Position: %A{finalPosition}\nAnswer: %d{answer}"
    
    module PartTwo =
        let movements =
            lines |> Seq.map parseCommandToVector3
        let updatePosition currentPosition courseChange =
            match (courseChange, currentPosition) with
            | (x1, y1, _), (x2, y2, z) -> Vector3 (x1+x2, y2+(x1*z), z+y1)
        let finalPosition = (makeVector3 0 0 0, movements) ||> Seq.fold updatePosition
        let answer =
            match finalPosition with
            | x,y,_ -> x * y
        
        printfn $"PART TWO\nFinal Position: %A{finalPosition}\nAnswer: %d{answer}"
 