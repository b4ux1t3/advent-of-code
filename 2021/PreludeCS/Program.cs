﻿// See https://aka.ms/new-console-template for more information

using BigramParsing;

Console.WriteLine("Hello, World!");
var inFile = File.OpenRead(args[0]);
using StreamReader reader = new StreamReader(inFile);
var inputText = reader.ReadToEnd();

var parser = new Parser(inputText);

parser.SanitizeText();
parser.ParseText();

var outFile = File.OpenWrite("output.txt");
using StreamWriter writer = new StreamWriter(outFile);

foreach (var bigramValuesKey in parser.BigramValues.Keys)
{
    writer.WriteLine($"<{bigramValuesKey.Item1},{bigramValuesKey.Item2}>:{parser.BigramValues[bigramValuesKey]}");
}
