using System.Net.Mime;
using System.Text.RegularExpressions;

namespace BigramParsing{
    public class Parser {
        public Dictionary<Tuple<string, string>, int> BigramValues {get; protected set;}
        private Regex alphabet = new Regex("[^a-zA-Z0-9 -]"); // For input sanitization.

        public string InputText {get; set;}

        public Parser(string input){        
            // Initialize our dictionary.
            BigramValues = new Dictionary<Tuple<string, string>, int>();
            InputText = input;
        }
        public void SanitizeText() {
            // We're cleansing our text of punctuation and any newlines, and also setting everything to lower case.
            // Obviously this would need better cleaning for non-English localizations.
            InputText = alphabet.Replace(InputText, "").ToLower();
        }
        public bool ParseText(){
            // Split the text by spaces.
            var splitText = InputText.Split(" ");
            if(splitText.Length > 1){
                // Declare the placeholder Tuple for our loop. This is just to prevent garbage collection.
                Tuple<string, string> currentBigram;
                // Loop through splitText, starting at the second element, and start putting values into our dictionary.
                for (int i = 1; i < splitText.Length; i++){
                    // Generate the tuple
                    currentBigram = Tuple.Create<string, string>(splitText[i - 1], splitText[i]);

                    // Lookup the tuple in our dictionary
                    if (BigramValues.ContainsKey(currentBigram)) {
                        BigramValues[currentBigram]++;
                    } else {
                        BigramValues.Add(currentBigram, 1);
                    }
                }
                // If all this works, we'll return true
                return true;
            }
            // otherwise, we'll let our viewmodel know that we didn't succeed.
            return false; 
        }
        public void Clear(){
            BigramValues = new Dictionary<Tuple<string, string>, int>();
        }
    }
}