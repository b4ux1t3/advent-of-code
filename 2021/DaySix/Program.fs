namespace AdventOfCode.Year2021
module DaySix =
    open System
    open AdventOfCode.Utilities
    let inputs = InputGetter.DumpInput(Environment.GetCommandLineArgs(), 1).Split(',') |> Seq.map uint64 |> List.ofSeq
    let sum x y = x+y
    let blankState = [0..8] |> List.map (fun _ -> uint64 0)
    let loadLifeSpan state lifespan =
        let mutable i: uint64 = uint64 -1
        match state with
        | [_;_;_;_;_;_;_;_;_] -> state |> List.map (fun (n: uint64) ->
            i <- i + uint64 1
            if i = lifespan then n+uint64 1 else n
            )
        | _ -> state
    let incrementState state =
        match state with
        | [a;b;c;d;e;f;g;h;i] -> [b;c;d;e;f;g;h+a;i;a]
        | _ -> state
    let incrementStateNTimes n state =
        (state,[0..n-1]) ||> List.fold (fun currentState _ -> incrementState currentState)
    let initialState =
        (blankState, inputs)
        ||> List.fold loadLifeSpan
    
    module PartOne =
        let numDays = 80
        let finalState = incrementStateNTimes numDays initialState
        let answer = finalState |> List.reduce sum
        printfn $"%d{answer}"
        
    module PartTwo =
        let numDays = 256
        let finalState = incrementStateNTimes numDays initialState
        let answer = finalState |> List.reduce sum
        printfn $"%d{answer}"
        
    module BonusRound =
        let numDays = 8192
        let finalState = incrementStateNTimes numDays initialState
        let answer = finalState |> List.reduce sum
        printfn $"%d{answer}"