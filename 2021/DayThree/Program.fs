namespace AdventOfCode.Year2021
module DayThree =
    open System
    open AdventOfCode.Utilities
    let inputs =
        InputGetter.GetInputs(Environment.GetCommandLineArgs(), 1)
        |> List.ofArray
        |> List.map (fun input ->
            match input |> List.ofSeq with
            | [a;b;c;d;e;f;g;h;i;j;k;l] -> ((int a)-48,(int b)-48,(int c)-48,(int d)-48,(int e)-48,(int f)-48,(int g)-48,(int h)-48,(int i)-48,(int j)-48,(int k)-48,(int l)-48)
            |_ -> (0,0,0,0,0,0,0,0,0,0,0,0) 
        )
    let add x y =
        match x,y with
        | (a,b,c,d,e,f,g,h,i,j,k,l),(m,n,o,p,q,r,s,t,u,v,w,x) -> a+m,b+n,c+o,d+p,e+q,f+r,g+s,h+t,i+u,j+v,k+w,l+x
    let greaterThanHalf x y =
        if x > y/2 then
            1
        else
            0
    let findMostCommonValue countsOfDigits numInputs =
        match countsOfDigits with
        | (a,b,c,d,e,f,g,h,i,j,k,l) ->
            (greaterThanHalf a numInputs,
             greaterThanHalf b numInputs,
             greaterThanHalf c numInputs,
             greaterThanHalf d numInputs,
             greaterThanHalf e numInputs,
             greaterThanHalf f numInputs,
             greaterThanHalf g numInputs,
             greaterThanHalf h numInputs,
             greaterThanHalf i numInputs,
             greaterThanHalf j numInputs,
             greaterThanHalf k numInputs,
             greaterThanHalf l numInputs)
    let flipBit bit = if bit = 1 then 0 else 1
    let flipDigits digits =
        match digits with
        | a,b,c,d,e,f,g,h,i,j,k,l ->
            (flipBit a,
             flipBit b,
             flipBit c,
             flipBit d,
             flipBit e,
             flipBit f,
             flipBit g,
             flipBit h,
             flipBit i,
             flipBit j,
             flipBit k,
             flipBit l)
    let collapseToInt digits =
        match digits with
        | a,b,c,d,e,f,g,h,i,j,k,l ->
            (a<<<11)+(b<<<10)+(c<<<9)+(d<<<8)+(e<<<7)+(f<<<6)+(g<<<5)+(h<<<4)+(i<<<3)+(j<<<2)+(k<<<1)+(l<<<0)
    let numberOfInputs = inputs.Length
    let countsPerDigit =
        inputs |> List.reduce add
    printfn $"Counts: %A{countsPerDigit}"
    let gamma = findMostCommonValue countsPerDigit numberOfInputs
    let epsilon = flipDigits gamma
    printfn $"Gamma:\t\t%A{gamma}=%d{collapseToInt gamma}\nEpsilon:\t%A{epsilon}=%d{collapseToInt epsilon}"
    
    module PartOne =
        let answer = collapseToInt gamma * collapseToInt epsilon
    
        printfn $"%d{answer}"
    module PartTwo =
        let checkIfBitMatches index bitsToCheck bits =
            let checkList =
                match bitsToCheck with
                | a,b,c,d,e,f,g,h,i,j,k,l -> [a;b;c;d;e;f;g;h;i;j;k;l]
            match bits with
            | a,b,c,d,e,f,g,h,i,j,k,l ->
                let bitList = [a;b;c;d;e;f;g;h;i;j;k;l]
                bitList.[index] = checkList.[index]
        let filterListWithMatches index bitsToCheck bitsList =
            let bitCheckLIst = bitsToCheck |> findMostCommonValue
            let checkFn = checkIfBitMatches index bitsToCheck
            bitsList |> findMostCommonValue |> List.filter checkFn
            
        let checkOne = filterListWithMatches 0 gamma
        let checkTwo = filterListWithMatches 1 gamma
        let checkThree = filterListWithMatches 2 gamma
        let checkFour = filterListWithMatches 3 gamma
        let checkFive = filterListWithMatches 4 gamma
        let checkSix = filterListWithMatches 5 gamma
        let checkSeven = filterListWithMatches 6 gamma
        let checkEight = filterListWithMatches 7 gamma
        let checkNine = filterListWithMatches 8 gamma
        let checkTen = filterListWithMatches 9 gamma
        let checkEleven = filterListWithMatches 10 gamma
        let checkTwelve = filterListWithMatches 11 gamma
        let filtered =
            inputs
            |> checkOne
            |> checkTwo
            |> checkThree
            |> checkFour
            |> checkFive
            |> checkSix
            |> checkSeven
            |> checkEight
        printfn $"%A{filtered}"