﻿# Advent of Code 2021 - Prelude

In this project, I'm simply getting a feel for the F# language. I'm not an F# expert by any stretch of the imagination, and functional first programming is also a fairly new concept to me. In fact, the vast majority of my functional programming knowledge comes from Computerphile videos and is entirely _conceptual_.

I need to get used to F# before I start using it on a daily basis, and then I'll hone the skills throughout December by finishing the AoC puzzles. To do the former, I've settled on a pretty simple challenge, one that requires very little practical knowledge to get started, but which flexes a few common programming skills.

## The Challenge

This Prelude challenge is half of the programming interview for the company I work for: Bigram Counting. For more info, check out the Wikipedia page on [N-Grams](https://en.wikipedia.org/wiki/N-gram).

This bigram (n-gram where n=2) counter will be counting simple lexical tokens corresponding to English-like words, matching the regular expression `[^]\[\s\.,:()\\/"+*=_``^]+`. (If you're wondering why I'm not just using `\w+`, that would include things like underscores, and would _not_ include things like hyphens.)

I intend to add a contraction lookup, so that I can track the individual words ("I'm", one lexical token, becomes "I am", two lexical tokens). This will be considered the "two star" version of this challenge, per AoC terms.

I _might_ also extend it to consider parentheticals, comma-separated clauses, and other discrete linguistic groupings (like sentences!) separately, but that's a stretch goal.

## What is this going to accomplish?
This task requires a few things:
1. It means I'll have to write a simple lexer in F#, which is commonly a helpful tool in AoC.
2. It means I'll have to learn how collections work in F#. I know F# has access to all the collections in .NET (since that's, you know, the whole point), but I want to make sure I'm using them "correctly".
3. It means I'll have to be at least somewhat conscious of performance if I want to count bigrams in any input of appreciable size before the heat death of the universe.

# Program Structure

We'll be using something called parser combinators, a really cool term for something relatively mundane: a parser that has a lexer built right in. Given a lexer is traditionally a program that goes byte by byte through an array of characters, and outputs tokens as it goes, and given that functional programming _abhors_ any kind of iteration (because that's a procedure), a parser combinator is a logical choice.

The individual parsers that I'm going to be building are relatively simple:

1. `string -> string list`
   * Take our string (in the form of a file input), and tokenize it.
2. `string list -> tuple<string, string> list`
   * Take those tokens, and return a list of pairs.
3. `tuple<string, string> list -> dictionary<tuple<string ,string>, int>`
   * Count each unique list of pairs, and record the result in a dictionary.

In aggregate, when these are combined using a parser combinator, this will give us a parser that looks like this:

```text
string -> dictionary<tuple<string, string>, int>
```