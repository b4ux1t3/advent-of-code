﻿module public Parsers

    open System
    open System.Text.RegularExpressions
    type Result<'a> =
        | Success of 'a
        | Failure of string

    type Parser<'a> = Parser of (string -> Result<'a * string>)

    let run parser input =
        let (Parser innerFn) = parser
        innerFn input

    // Simple Parser
    let pchar charToMatch =
        let innerFn (input: string) = 
            if String.IsNullOrEmpty input then
                Failure "No more input"
            else
                let first = input.[0]
                if first = charToMatch then
                    let remaining = input.[1..]
                    Success (charToMatch, remaining)
                else
                    let msg = $"Expecting '%c{charToMatch}'. Got '%c{first}'"
                    Failure msg
        Parser innerFn
    let pregex regex =
        let innerFn input =
            let matches = Regex.Matches(input,regex)
            match matches.Count with
            | 0 ->
                Failure "No matches found in input text"
            | _ ->
                let parsedMatches = matches
                Success (parsedMatches, "")
                
        Parser innerFn
    // End simple parser

    // Parser combinators
    let andThen parser1 parser2 =
        let innerFn input =
            let result1 = run parser1 input
            
            match result1 with
            | Failure err ->
                Failure err
            | Success (value1,remaining1) ->
            
                let result2 = run parser2 remaining1
            
                match result2 with
                | Failure err ->
                    Failure err
                | Success (value2, remaining2) ->
                    let combinedValue = (value1,value2)
                    Success (combinedValue,remaining2)
        Parser innerFn

    let ( .>>. ) = andThen
        
    let orElse parser1 parser2 =
        let innerFn input =
            let result1 = run parser1 input
            
            match result1 with
            | Success _ ->
                result1
            | Failure _ ->
                let result2 = run parser2 input
                result2
        Parser innerFn
    let ( <|> ) = orElse

    let mapP f parser =
        let innerFn input =
            let result = run parser input
            
            match result with
            | Success (value, remaining) ->
                let newValue = f value
                Success (newValue, remaining)
            | Failure  err ->
                Failure err
        Parser innerFn
        
    let ( |>> ) x f = mapP f x
    
    let choice listOfParsers =
        listOfParsers |> List.reduce( <|> )
    
    let anyOf listOfChars =
        listOfChars
        |> List.map pchar
        |> choice
        
    let parseLowerCase = anyOf ['a'..'z']
    let parseDigit = anyOf ['0'..'9']
    
    let sequence listOfParsers =
        let concatResults p1 p2 =
            p1 .>>. p2
            |>> (fun(list1,list2) -> list1@list2)
        listOfParsers
        |> Seq.map(fun parser -> parser |>> List.singleton)
        |> Seq.reduce concatResults
    
    let pstring str =
        str
        |> Seq.map pchar
        |> sequence
        |>> List.toArray
        |>> String

module TestBasicCombinators =
    open Parsers
    let parseA = pchar 'A'
    let parseB = pchar 'B'
    let parseC = pchar 'C'
    let inputABC = "ABC"
    
    let parseAThenB = parseA .>>. parseB
    
    let blah = run parseAThenB inputABC
    let parseAll = parseAThenB .>>. parseC
    let blah2 = run parseAll inputABC