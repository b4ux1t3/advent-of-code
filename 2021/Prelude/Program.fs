open System
open System.Collections.Generic

module Prelude =
    open System
    open System.Text.RegularExpressions
    open System.IO
    type Token<'a> = Token of 'a
    type Bigram<'a> = (Token<'a> * Token<'a>)
    type Count<'a> = Count of ('a * int)
    // Initialize our data
    let args = Environment.GetCommandLineArgs()
    let searchText = File.ReadAllText args[1]
    let regex = "[^]\[\s\.,:()\\\/\"+*=_`^]+"
    
    // Find all of the tokens we're looking for.
    let tokenizeString tokenRegex input =
        Regex.Matches(input, tokenRegex)
                 |> Seq.cast
                 |> Seq.map (fun (x: Match) -> Token x.Value)
                 |> Seq.toList
    let matches = tokenizeString regex searchText
    
    // Find our bigrams
    // Apparently this already exists:
    // https://fsharp.github.io/fsharp-core-docs/reference/fsharp-collections-listmodule.html#zip
    // I'm leaving it here anyway, because I want to represent bigrams specifically.
    let zip list1 list2 =
        (list1,list2) ||> List.map2 (fun el1 el2 -> Bigram (Token el1, Token el2))
    let getBigrams<'a> (orderedListOfTokens: Token<'a> list) =
        let lastIndex = orderedListOfTokens.Length-2 // Slicing is inclusive, apparently
        zip orderedListOfTokens.[..lastIndex] orderedListOfTokens.[1..]
    
    // Count bigrams
    let countDictionary = new Dictionary<Bigram<Token<string>>, int>()
    let bigrams = getBigrams matches
    
    let countOccurrences<'a> listOfItems (dictionary: Dictionary<'a, int>) =
        listOfItems |> List.iter (fun x ->
                if dictionary.ContainsKey(x) then dictionary[x] <- dictionary[x] + 1
                else dictionary.Add(x, 1)
            ) 
    let output filename =
        use file = File.CreateText(filename)
        let keys = countDictionary.Keys |> List.ofSeq
        ("",keys) ||> List.fold (fun state key ->
                state +  $"%A{key}: %d{countDictionary[key]}\n"
            )
        |> file.Write
        
    let finish =
        let _ = countOccurrences bigrams countDictionary
        output "output.txt"
    
    