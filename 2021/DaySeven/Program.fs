namespace AdventOfCode.Year2021
module DaySeven =
    open System
    open AdventOfCode.Utilities
    let sum x y = x + y
    let compoundSum x y z = x + ([0..y] |> List.reduce sum)
    let inputs =
        InputGetter.GetInputs(Environment.GetCommandLineArgs(), 1, ",".ToCharArray())
        |> List.ofSeq
        |> List.map int
    let calcMoveCost destination source =
        (max destination source) - (min destination source)
    let calcCostsForSpace destination positions =
        let calcMoveCostForDestination = calcMoveCost destination
        positions |> List.map calcMoveCostForDestination
    let calcAllDestinations calcFn spaces positions =
        spaces |> List.map (fun space ->
            positions |> calcCostsForSpace space |> List.reduce calcFn
        )
    let minimum = inputs |> List.reduce min
    let maximum = inputs |> List.reduce max
    let spaces = [minimum..maximum]
        
    module PartOne =
        let calcAllDestinationsWithSum = calcAllDestinations sum
        let calcWithRealSpaces = calcAllDestinationsWithSum spaces
        let distancesToSpaces =
            inputs |> calcWithRealSpaces
        let answer = distancesToSpaces |> List.reduce min
        printfn $"Part One: %d{answer}"
        
    module PartTwo =
        let sumRange n = [0..n] |> List.reduce sum
        let calcAllDestinationsPartTwo calcFn spaces positions =
            spaces |> List.map (fun space ->
                positions
                |> calcCostsForSpace space
                |> List.map sumRange
                |> List.reduce calcFn
            )
        let calcAllDestinationsWithCompoundSum = calcAllDestinationsPartTwo sum
        let calcWithRealSpaces = calcAllDestinationsWithCompoundSum spaces
        let distancesToSpaces =
            inputs |> calcWithRealSpaces
        let answer = distancesToSpaces |> List.reduce min
        printfn $"Part Two: %d{answer}"