module DayOne =
    open System 
    open System.IO
    let filename = Environment.GetCommandLineArgs()[1]
    let inputLines = File.ReadLines filename |> List.ofSeq
    let parsedLines = inputLines |> List.map (Int32.Parse)
    let countChanges comparator listOfBigrams =
        (0,listOfBigrams) ||> List.fold (fun state value ->
                match value with
                | (val1, val2) -> if comparator val1 val2 then state+1 else state
        )
    let decreaseComparator val1 val2 = val1 > val2
    let increaseComparator val1 val2 = val1 < val2
    let zipListToBigrams (listOfItems: 'a list) =
        (
            listOfItems.[..listOfItems.Length-2],
            listOfItems.[1..]
        ) ||> List.zip
    let zipListToTrigrams (listOfItems: 'a list) =
        (
            listOfItems.[..listOfItems.Length-3],
            listOfItems.[1..listOfItems.Length-2],
            listOfItems.[2..]
        ) |||> List.zip3
    let sumOfTrigram trigram =
        match trigram with
        | (a,b,c) -> a + b + c
        
    module PartOne =
        let bigrams = zipListToBigrams parsedLines
        let partOneSolution = countChanges increaseComparator bigrams
        let finishPartOne = printfn $"Number of increases in Part One: %d{partOneSolution}"
    
    module PartTwo =
        let trigrams = zipListToTrigrams parsedLines
        let trigramSums = trigrams |> List.map sumOfTrigram
        let bigramsOfTrigrams = zipListToBigrams trigramSums
        let partTwoSolution = countChanges increaseComparator bigramsOfTrigrams
        let finishPartTwo = printfn $"Number of increases in Part Two: %d{partTwoSolution}"
        