namespace AdventOfCode.Year2021

module DayFive =
    open System
    open System.Text.RegularExpressions
    open System.Collections.Generic
    open AdventOfCode.Utilities
    
    type Point = int*int
    type Line = Point*Point
    let makeLine x1 y1 x2 y2 =
        Line ((Point (x1, y1)),(Point (x2, y2)))
    let lineRegex = @"(?<x1>\d+),(?<y1>\d+)\s+->\s+(?<x2>\d+),(?<y2>\d+)"    
    let parseLine pattern line =
        let matchList = Regex.Match(line, pattern).Groups.Values|> List.ofSeq
        let matches = matchList.[1..] |> List.map (fun group -> int group.Captures[0].Value)
        match matches with
        | [x1; y1; x2; y2] -> makeLine x1 y1 x2 y2
        | _ -> makeLine 0 0 0 0
    let parseLineWithPattern = parseLine lineRegex
    // Line 1 has to be vertical, line 2 has to be horizontal for both of these functions
    let linesIntercept line1 line2 =
        match line1, line2 with
        | ((x1,y1),(_,y2)),((x3,y3),(x4,_)) ->
            ((x3<=x1&&x1<=x4)||(x3>=x1&&x1>=x4))&&((y2<=y3&&y3<=y1)||(y2>=y3&&y3>=y1))
    let getLineIntercept line1 line2  =
        match line1, line2 with
        | ((x1,_),(_,_)),((_,y3),(_,_)) -> Point (x1,y3)
    let lineIsVertical line =
        match line with
        | (x1,_),(x2,_) -> x1 = x2
    let lineIsFlat line =
        match line with
        | (_,y1),(_,y2) -> y1 = y2
    let getHorizontalLinePoints line =
        match line with
        | (x1,y), (x2, _) ->
            let makePointWithX x = Point (x, y)
            [(min x1 x2)..(max x1 x2)] |> List.map makePointWithX
    let getVerticalLinePoints line =
        match line with
        | (x,y1), (_, y2) ->
            let makePointWithY y = Point (x, y)
            [(min y1 y2)..(max y1 y2)] |> List.map makePointWithY
    let getSlopedLinePoints line =
        match line with
        | (x1,y1) , (x2,y2)->
            let xValues = if x1 < x2 then [x1..x2] else [x1.. -1..x2]
            let yValues = if y1 < y2 then [y1..y2] else [y1.. -1..y2]
            (xValues, yValues) ||> List.zip
    let getLinePoints (line: Line) =
        if lineIsFlat line then
            getHorizontalLinePoints line
        else if lineIsVertical line then
            getVerticalLinePoints line
        else
            getSlopedLinePoints line
    // Input parsing
    let lines = InputGetter.GetInputs(Environment.GetCommandLineArgs(), 1) |> List.ofSeq |> List.map parseLineWithPattern
    let addToDictionary (dict:Dictionary<Point,int>) pointToAdd  =
        if dict.ContainsKey pointToAdd then
            dict[pointToAdd] <- dict[pointToAdd] + 1
        else
            dict[pointToAdd] <- 1
    let pointDictionary = Dictionary<Point, int>()
    let addToOurDict = addToDictionary pointDictionary
    let makePointsForLine line =
        let points = getLinePoints line
        points |> List.iter addToOurDict
    let filterPointTuples num (kvp: KeyValuePair<Point, int>) =
        match kvp with
        | kvp  -> kvp.Value >= num
    let filterCountTwo = filterPointTuples 2
    
    module PartOne =
        let horizontalLines = lines |> List.filter lineIsFlat
        let verticalLines = lines |> List.filter lineIsVertical
        let generateDict =
            horizontalLines |> List.map makePointsForLine |> ignore
            verticalLines |> List.map makePointsForLine |> ignore
        let points = pointDictionary |> List.ofSeq |> List.filter filterCountTwo
        
        printfn $"Part One: %d{points.Length}"
    
    module PartTwo =
        let restOfTheLines = lines |> List.filter (fun line -> not(lineIsFlat line) && not (lineIsVertical line))
        let generateDict =
            restOfTheLines |> List.map makePointsForLine |> ignore
        
        let points = pointDictionary |> List.ofSeq |> List.filter filterCountTwo
        printfn $"Part Two: %d{points.Length}"
