﻿using System.Text.RegularExpressions;
using AdventOfCode.Utilities;
using AdventOfCode.Year2021;
var inputString = InputGetter.DumpInput(args);
bool parallel = !(args.Length > 1);
var inputRegex = @"(\d{1,2},?)+\n\n";
var bingoRegex =@"(?<BingoCards>(\s*\d{1,2}\s+\d{1,2}\s+\d{1,2}\s+\d{1,2}\s+\d{1,2}\n){5})";
var inputs = Regex.Match(inputString, inputRegex).Captures.FirstOrDefault()?.Value.Split(',').Select(int.Parse).ToArray();
var bingoCards = Regex.Matches(inputString, bingoRegex).Select(match => match.Value).Select(ParseBingoCard).Select(b => new BingoCard(b)).ToList();
var timer = DateTime.Now;
ParallelOptions parallelOptions = new()
{
    MaxDegreeOfParallelism = 12
};
ParallelLoopResult parallelLoopResult = new ParallelLoopResult();
if (parallel)
{
    parallelLoopResult = Parallel.ForEach(bingoCards, parallelOptions, card => card.EvaluateCard(inputs));
}
else
{
    bingoCards.ForEach(c=>c.EvaluateCard(inputs));
}

if (parallelLoopResult.IsCompleted || !parallel)
{
    var finishTime = DateTime.Now;
    var successfulCards = bingoCards.Where(card => card.Result is {Win: true}).ToList();
    successfulCards.Sort((card, bingoCard) =>
        bingoCard == null || card == null? 0 : bingoCard.Result.Turn < card.Result.Turn ? 1 : -1);
    Console.WriteLine($"Part One: {successfulCards[0].Result.LastNumber * successfulCards[0].Result.sumOfNums}");
    Console.WriteLine($"Part Two: {successfulCards[successfulCards.Count-1].Result.LastNumber * successfulCards[successfulCards.Count-1].Result.sumOfNums}");
    Console.WriteLine($"Execution in {(parallel?"parallel":"series")} took {finishTime - timer}");      
}

int[] ParseBingoCard(string bingoInput)
{
    var cleanedStrings = Regex.Replace(bingoInput, @"[\s\n]+", " ").Split(' ').Where(s=>s!=string.Empty);
    return cleanedStrings.Select(int.Parse).ToArray();
}

internal class BingoCard
{
    public int[][] Board { get; }
    public bool[][] Matches { get; }
    public Result? Result { get; internal set; }
    public int SumOfUnmarkedNumbers { get; internal set; }
    internal BingoCard(IReadOnlyList<int> data)
    {
        SumOfUnmarkedNumbers = data.Sum();
        Board = new int[5][];
        Matches = new bool[5][];
        var i = 0;
        for (; i < 5; i++)
        {
            Board[i] = new int[5];
            Matches[i] = new bool[5];
        }

        for (i = 0; i < data.Count; i++)
        {
            Board[i / 5][i % 5] = data[i];
            Matches[i / 5][i % 5] = false;
        }
    }

    public void EvaluateCard(int[]? numbers)
    {
        if (numbers != null)
            for (var i = 0; i < numbers.Length; i++)
            {
                var result = CheckNumber(numbers[i]);
                if (result == -1) continue;
                Matches[result / 5][result % 5] = true;
                SumOfUnmarkedNumbers -= numbers[i];
                if (CheckWin(result) && Result == null)
                {
                    Result = new Result(true, i+1, numbers[i], SumOfUnmarkedNumbers);
                }
            }
        Result ??= new Result(false, -1, -1, -1);
    }
    private int CheckNumber(int num)
    {
        for (var row = 0; row < Board.Length; row++)
        {
            for (var col = 0; col < Board[row].Length; col++)
            {
                if (Board[row][col] == num) return row * Board.Length + col;
            }
        }
        return -1;
    }
    private bool CheckWinRow(int row)
    {
        return Matches[row].Count(b => b) == 5;
    }
    private bool CheckWinCol(int col)
    {
        return Matches.Select(r => r[col]).Count(b=>b) == 5;
    }
    private bool CheckWin(int index)
    {
        return CheckWinRow(index / Board.Length) || CheckWinCol(index % Board.Length);
    }
}