﻿namespace AdventOfCode.Year2021;
internal record Result(bool Win, int Turn, int LastNumber, int sumOfNums);