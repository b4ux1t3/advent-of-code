namespace AdventOfCode.Year2021
module DayEight =
    open System
    open AdventOfCode.Utilities
    let sum x y = x+y
    let inputs = InputGetter.GetInputs(Environment.GetCommandLineArgs(), 1) |> List.ofSeq
    let filterEmpty list =
        list |> List.filter (fun (s: string) -> s<>"")
    let splitOnBar (line: string) =
        line.Split('|') |> List.ofSeq |> filterEmpty
    let splitOnSpace (line: string) =
        line.Split(' ') |> List.ofSeq |> filterEmpty
    let parseLine (line: string) =
        splitOnBar line |> List.map splitOnSpace 
    let stringIsNLength n (s: string) =
        s.Length = n
    let lines =
        inputs |> List.map parseLine |> List.map (fun lst -> (lst.[0],lst.[1]))
    module PartOne =
        let length2 = stringIsNLength 2
        let length3 = stringIsNLength 3
        let length4 = stringIsNLength 4
        let length7 = stringIsNLength 7
        let partOneValidOutput str =
            (length2 str) || (length3 str) || (length4 str) || (length7 str)
        let outputs =
            lines |> List.map (fun line ->
                match line with
                | _, outputs -> outputs)
        let answer =
            outputs
            |> List.map (fun lst ->
                    let filteredList = lst |> List.filter partOneValidOutput
                    filteredList.Length
                )
            |> List.reduce sum
        printfn $"Part One: %d{answer}"  
        
   
    