namespace AdventOfCode.Year2021

open System
open System.Collections.Generic
open AdventOfCode.Utilities

module DayTen =
    let inputs = 
        InputGetter.GetInputs(Environment.GetCommandLineArgs(), 1)
    type Token =
        | Opener
        | Closer
    let parseToken token =
        match token with
        | '(' -> Opener
        | '[' -> Opener
        | '{' -> Opener
        | '<' -> Opener
        | ')' -> Closer
        | ']' -> Closer
        | '}' -> Closer
        | '>' -> Closer
        | _ -> Closer
    let OpeningLookup = Dictionary<char,char>()
    OpeningLookup.Add('(', ')')
    OpeningLookup.Add('[', ']')
    OpeningLookup.Add('{', '}')
    OpeningLookup.Add('<', '>')
    let ScoreLookup = Dictionary<char,int>()
    ScoreLookup.Add(')', 3)
    ScoreLookup.Add(']', 57)
    ScoreLookup.Add('}', 1197)
    ScoreLookup.Add('>', 25137)
    let CallStack = Stack<char>()
    let ScoreList = List<char>()
    let pushToStack (stack: Stack<'a>) thingToPush =
        stack.Push(thingToPush)
        true
    let checkStack (dictionary: Dictionary<'a,'b>) (stack: Stack<'a>) thingToCheck =
        dictionary.[stack.Pop()] = thingToCheck
    let addToList (lst: List<'a>) invalidChar =
        lst.Add(invalidChar)
        true
    let pushToOurStack = pushToStack CallStack
    let popOurStack = checkStack OpeningLookup CallStack
    let addToOurList = addToList ScoreList
    
    module PartOne =
        let execute store inp =
            match (parseToken inp) with
            | Opener -> pushToOurStack inp
            | Closer ->
                match (popOurStack inp), store with
                | false, true -> not (addToOurList inp)
                | true, true -> true
                | _ -> false
        let executeOnString str =
            (true, str)
            ||> Seq.fold (fun state value ->
                    match state with
                    | true -> execute state value
                    | false -> false
                )
        let filteredList =
            inputs |> Array.filter executeOnString
        let answer =
            let results = ScoreList |> List.ofSeq 
            (0, results)
            ||> List.fold (fun state value ->
                state + (ScoreLookup[value]))
            
        printfn $"Part One: %d{answer}"
        
        module PartTwo =
            // For part two, we need individual stacks for each line!
            let execute (stack: Stack<char>) inp =
                match(parseToken inp) with
                | Opener -> stack.Push(inp)
                | Closer -> stack.Pop() |> ignore
            let ScoreLookup = Dictionary<char,uint64>()
            ScoreLookup.Add(')', uint64 1)
            ScoreLookup.Add(']', uint64 2)
            ScoreLookup.Add('}', uint64 3)
            ScoreLookup.Add('>', uint64 4)
            let addScore x y = (x * (uint64 5)) + y
            let lookupScore x = ScoreLookup[x]
            let parseLine str =
                let localStack = Stack<char>()
                str
                |> Seq.iter (execute localStack)
                let remaining = localStack.ToArray() |> Array.map (fun i -> OpeningLookup[i])  |> Array.map lookupScore 
//                remaining |> Array.Reverse
                remaining |> Array.reduce addScore
            
            let scores = filteredList |> Array.map parseLine |> Array.sort
            
            let index = scores.Length / 2
            let answer = scores[index]
            
            printfn $"Part Two: %d{answer}"
                