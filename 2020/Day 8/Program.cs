﻿using System;
using System.Collections.Generic;
using AdventOfCode.Utilities;

namespace AdventOfCode.Year2020.Day8
{
    class Program
    {   
        static int programCounter = 0;
        static int acc = 0;
        static bool halt = false;
        static bool jumped = false;
        static bool looped = false;
        static void Jmp(int pointer)
        {
            programCounter += pointer;
            jumped = true;
        }
        static void Acc(int number)
        {
            acc += number;
        }
        
        static void ExecuteInstruction(string instruction)
        {
            string[] splitString = instruction.Split(" ");
            string opCode = splitString[0];
            int argument = Convert.ToInt32(splitString[1]);
            if (opCode == "acc")
            {
                Acc(argument);
            }
            else if (opCode == "jmp")
            {
                Jmp(argument);
            }
        }
        // Returns true if a program loops, false otherwise.
        static bool ExecuteProgram(string[] inputs)
        {
            programCounter = 0;
            acc = 0;
            halt = false;
            jumped = false;
            looped = false;
            Dictionary<int, int> instructionRegister = new Dictionary<int, int>();

            while(!halt)
            {
                if (instructionRegister.ContainsKey(programCounter)) instructionRegister[programCounter]++;
                else instructionRegister.Add(programCounter, 1);
                looped = instructionRegister[programCounter] == 2;
                if (looped) Console.WriteLine($"Loop detected.\nInstruction {programCounter} {inputs[programCounter]} run {instructionRegister[programCounter]} times.\nValue of ACC: {acc}");
                ExecuteInstruction(inputs[programCounter]);
                if (jumped)
                {
                    jumped = false;
                }
                else programCounter++;
                halt = looped | programCounter == inputs.Length;
            }
            return halt && looped;
        }

        static string[] FlipInstance(string[] inputs, int instance)
        {
            int count = 0;
            int index = 0;
            bool matched = false;
            string[] opcodesToFlip = {"nop", "jmp"};
            string[] returnStrings = new string[inputs.Length];
            string[] currentInstruction;
            foreach (var instruction in inputs)
            {
                currentInstruction = instruction.Split(" ");
                if (currentInstruction[0] == opcodesToFlip[0] || currentInstruction[0] == opcodesToFlip[1] && !matched)
                {
                    if (count++ == instance)
                    {
                        if (currentInstruction[0] == opcodesToFlip[0]) returnStrings[index] = $"{opcodesToFlip[1]} {currentInstruction[1]}";
                        else if (currentInstruction[0] == opcodesToFlip[1]) returnStrings[index] = $"{opcodesToFlip[0]} {currentInstruction[1]}";
                        matched = true;
                    }
                    else returnStrings[index] = instruction;
                }
                
                else returnStrings[index] = instruction;
                index++;
            }
            return returnStrings;
        }
        static int CountInstances(string[] inputs)
        {
            int count = 0;
            string[] opcodesToCount = {"nop", "jmp"};
            foreach (var item in inputs)
            {
                string opcode = item.Substring(0,3);
                if (opcode == opcodesToCount[0] || opcode == opcodesToCount[1]) count++;
            }
            return count;
        }
        static void Main(string[] args)
        {
            string[] inputs = InputGetter.GetInputs(args);
            
            int instances = CountInstances(inputs);
            List<string[]> programsToCheck = new List<string[]>();
            for (int i = 0; i < instances; i++)
            {
                programsToCheck.Add(FlipInstance(inputs, i));
            }
            int index = 0;
            int correctIndex = 0;
            int correctACC = 0;
            foreach (var program in programsToCheck)
            {
                if ( !(ExecuteProgram(program)))
                {
                    correctIndex = index;
                    correctACC = acc;
                }
                index++;
            }
            if (correctIndex != null || correctACC != null) Console.WriteLine($"Correct program found (#{correctIndex}).\nACC after not looping: {correctACC}");
        }
    }
}
