﻿using System;
using System.Collections.Generic;
using AdventOfCode.Utilities;

namespace AdventOfCode.Year2020.Day3
{
    class Velocity
    {
        public int x { get; protected set; }
        public int y { get; protected set; }

        public Velocity(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

    }
    class Program
    {
        static void Main(string[] args)
        {
            Velocity[] velocities = { new Velocity(1, 1), new Velocity(3, 1), new Velocity(5, 1), new Velocity(7, 1), new Velocity(1, 2) };

            var inputLines = InputGetter.GetInputs(args);
            var originalLines = InputGetter.GetInputs(args);

            int width = inputLines[0].Length;
            int height = inputLines.Length;

            Console.WriteLine($"Puzzle Width:\t{width}\nPuzzle Height:\t{height}\n");

            char treeSpace = '#';
            int currentX = 0;
            int currentY = 0;
            long treeCount = 0;
            char currentCharacter = ' ';
            List<long> answers = new List<long>();

            foreach (var velocity in velocities)
            {
                currentX = 0;
                treeCount = 0;
                for (currentY = 0; currentY < inputLines.Length; currentY += velocity.y)
                {
                    currentCharacter = inputLines[currentY][currentX % width];
                    if (currentCharacter == treeSpace) treeCount++;
                    currentX += velocity.x;
                }
                answers.Add(treeCount);
            }

            long rollingProduct = 1;
            int index = 0;
            foreach (var number in answers)
            {
                Velocity currentVelocity = velocities[index++];
                rollingProduct *= number;
                Console.WriteLine($"Velocity {currentVelocity.x}, {currentVelocity.y} produces {number} hits.");
            }
            Console.WriteLine($"\nPart 2 Total hits:\t{rollingProduct}");

        }
    }
}
