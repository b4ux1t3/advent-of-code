lines = []

with open("input.txt", "r") as in_file:
    lines = in_file.readlines()

def gen_plane():
    return [(row, [column for column in range(8)]) for row in range(128)]

taken_seats = []
highest_seat = 0

for line in lines:
    plane = gen_plane()
    
    
    # Find the row
    for i in range(7):
        if line[i] == "F":
            plane = plane[:len(plane) // 2]
        else:
            plane = plane[len(plane ) // 2:]

    # Find the column
    seats = plane[0][1]
    for i in range(3):
        if line[7+i] == "L":
            seats = seats[:len(seats) // 2]
        else:
            seats = seats[len(seats) // 2:]

    # calculate the seat number
    seat_num = plane[0][0] * 8 + seats[0]
    taken_seats.append(seat_num) 
    if seat_num > highest_seat:
        highest_seat = seat_num



    
print(f"The highest seat is {highest_seat}.")

taken_seats.sort()
for i in range(min(taken_seats),max(taken_seats)):
    if i not in taken_seats:
        print(f"Your seat is {i}.")