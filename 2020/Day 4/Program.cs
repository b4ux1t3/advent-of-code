﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using AdventOfCode.Utilities;

namespace AdventOfCode.Year2020.Day4
{
    class Program
    {
        public enum CheckBits
        {
            CID,
            PID,
            ECL,
            HCL,
            HGT,
            EYR,
            IYR,
            BYR,
        }

        static Dictionary<string, CheckBits> BitLookup = new Dictionary<string, CheckBits>()
        {
            {"cid", CheckBits.CID},
            {"pid", CheckBits.PID},
            {"ecl", CheckBits.ECL},
            {"hcl", CheckBits.HCL},
            {"hgt", CheckBits.HGT},
            {"eyr", CheckBits.EYR},
            {"iyr", CheckBits.IYR},
            {"byr", CheckBits.BYR},
        };
        static string[] CleanInputs(string[] inputs)
        {
            List<string> outList = new List<string>();
            string workingString = "";
            int emptyCount = 0;
            bool isEmpty = false;
            string currentInput = "";

            for (int i = 0; i < inputs.Length; i++)
            {
                currentInput = inputs[i];
                isEmpty = currentInput == "";
                if (isEmpty) emptyCount++;
                else emptyCount = 0;
                if (emptyCount == 3 || i == inputs.Length - 1)
                {
                    outList.Add(workingString.Trim());
                    emptyCount = 0;
                    workingString = "";
                }
                else
                {
                    workingString += isEmpty ? " " : currentInput;
                }
            }

            return outList.ToArray();
        }
        static Dictionary<string, string> GetFields(string input)
        {
            string[] fields = input.Split(" ");
            Dictionary<string, string> splitFields = new Dictionary<string, string>();
            string[] splitString;
            foreach (var field in fields)
            {
                splitString = field.Split(":");
                splitFields.Add(splitString[0], splitString[1]);
            }
            return splitFields;
        }
        static byte ParseInputToByte(string input)
        {
            // Byte map: "byr" "iyr" "eyr" "hgt" "hcl" "ecl" "pid" "cid"
            //             7     6     5     4     3     2     1     0
            // First ID:   1     1     1     1     1     1     1     1 = 255, VALID
            // Sec ID:     1     1     1     1     1     1     1     1 = 255, VALID
            // 5th ID:     1     1     1     1     1     1     1     0 = 254, VALID. . .ish
            byte setBits = 0;
            
            Dictionary<string, string> splitFields = GetFields(input);
            foreach (var item in splitFields)
            {
                try
                {
                    setBits |= (byte)(1 << (int)BitLookup[item.Key]);
                }
                catch (KeyNotFoundException)
                {

                };
            }
            return setBits;
        }
        static Identification ParseInputToID(string input)
        {
            Dictionary<string, string> fields = GetFields(input);
            Regex heightRegex = new Regex("(\\d+)(cm|in)");
            int byr = 0;
            int iyr = 0;
            int eyr = 0;
            Tuple<int, string> hgt = null;
            string hcl = null;
            string ecl = null;
            string pid = null;
            string cid = null;
            foreach (var item in fields)
            {
                if (item.Key == "byr") byr = Convert.ToInt32(item.Value);
                if (item.Key == "iyr") iyr = Convert.ToInt32(item.Value);
                if (item.Key == "eyr") eyr = Convert.ToInt32(item.Value);
                if (item.Key == "hgt")
                {
                    MatchCollection matches = heightRegex.Matches(item.Value);
                    Match[] matchBuffer = new Match[10];
                    matches.CopyTo(matchBuffer, 0);
                    Match match = matchBuffer[0];
                    if (match != null) hgt = new Tuple<int, string>(Convert.ToInt32(match.Groups[1].Captures[0].Value), match.Groups[2].Captures[0].Value);
                }
                if (item.Key == "hcl") hcl = item.Value;
                if (item.Key == "ecl") ecl = item.Value;
                if (item.Key == "pid") pid = item.Value;
                if (item.Key == "cid") cid = item.Value;
            }

            return new Identification(byr, iyr, eyr, hgt, hcl, ecl, pid, cid);
        }
        static bool IsValid(Identification ident)
        {
            Regex hairRegex = new Regex("^#[0-9a-f]{6}$");
            Regex eyeRegex = new Regex("^amb|blu|brn|gry|grn|hzl|oth$");
            Regex passportRegex = new Regex("^\\d{9}$");

            bool birthYearRule = ident.BirthYear >= 1920 && ident.BirthYear <= 2002;
            bool issueYearRule = ident.IssueYear >= 2010 && ident.IssueYear <= 2020;
            bool expirationRule = ident.ExpirationYear >= 2020 && ident.ExpirationYear <= 2030;
            bool heightRule = false;
            if (ident.Height != null){
                heightRule = (ident.Height.Item2 == "cm" && (ident.Height.Item1 >= 150 && ident.Height.Item1 <= 193)) || (ident.Height.Item2 == "in" && (ident.Height.Item1 >= 59 && ident.Height.Item1 <= 76)); // Wowza.
            }
            bool hairColorRule = !(ident.HairColor == null) && hairRegex.IsMatch(ident.HairColor);
            bool eyeColorRule = !(ident.EyeColor == null) && eyeRegex.IsMatch(ident.EyeColor);
            bool passportIdRule = !(ident.PassportID == null) && passportRegex.IsMatch(ident.PassportID);

            return birthYearRule && issueYearRule && expirationRule && heightRule && hairColorRule && eyeColorRule && passportIdRule;
        }
        static void Main(string[] args)
        {
            char[] delimiter = Environment.NewLine.ToCharArray();
            var inputs = InputGetter.GetInputs(args, 0, delimiter);
            string[] cleanedInputs = CleanInputs(inputs);

            List<byte> results = new List<byte>();
            List<Identification> parsedIdents = new List<Identification>();
            int validPassportsPart1 = 0;
            int validPassportsPart2 = 0;
            byte parsedInput;
            Identification parsedIdent;

            foreach (var item in cleanedInputs)
            {
                parsedInput = ParseInputToByte(item);
                parsedIdent = ParseInputToID(item);
                results.Add(parsedInput);
                parsedIdents.Add(parsedIdent);
                if (parsedInput >= (byte)254) {
                    validPassportsPart1++;
                    if (IsValid(parsedIdent)) validPassportsPart2++;
                }
            }
            Console.WriteLine($"There are {validPassportsPart1} valid Part 1 passports.");
            Console.WriteLine($"There are {validPassportsPart2} valid Part 2 passports.");
        }
    }
    internal class Identification
    {
        public static string[] RequiredFields = { "byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid" };
        public static string[] OptionalFields = { "cid" };
        public int BirthYear;
        public int IssueYear;
        public int ExpirationYear;
        public Tuple<int, string> Height;
        public string HairColor;
        public string EyeColor;
        public string PassportID;
        public string CountryID;

        public Identification(int byr, int iyr, int eyr, Tuple<int, string> hgt, string hcl, string ecl, string pid, string cid)
        {
            BirthYear = byr;
            IssueYear = iyr;
            ExpirationYear = eyr;
            Height = hgt;
            HairColor = hcl;
            EyeColor = ecl;
            PassportID = pid;
            CountryID = cid;
        }

    }
}
