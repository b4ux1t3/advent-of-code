﻿using System;
using System.IO;
using System.Collections.Generic;
using AdventOfCode.Utilities;

namespace AdventOfCode.Year2020.Day1
{
    class Program
    {
        static int[] ParseInputs(string[] inputs)
        {
            int[] returnArray = new int[inputs.Length];
            int index = 0;
            foreach (string input in inputs)
            {
                returnArray[index++] = Convert.ToInt32(input);
            }

            return returnArray;
        }

        static void Main(string[] args)
        {
            string[] inputs = InputGetter.GetInputs(args);
            int[] parsedInputs = ParseInputs(inputs);

            int comparisons = 0;
            for (int i = 0; i < parsedInputs.Length; i++)
            {
                for (int j = 0; j < parsedInputs.Length; j++)
                {
                    if (args.Length > 1 && args[1] == "true") // Part 2
                    {
                        for (int k = 0; k < parsedInputs.Length; k++)
                        {
                            comparisons++;
                            if (parsedInputs[j] == parsedInputs[i] || parsedInputs[j] == parsedInputs[k] || parsedInputs[k] == parsedInputs[i]) continue;
                            if (parsedInputs[j] + parsedInputs[i] + parsedInputs[k] == 2020)
                            {
                                Console.WriteLine($"{parsedInputs[j]} (Item #{j+1}) + {parsedInputs[i+1]} (Item #{i}) + {parsedInputs[k]} (Item #{k+1}) = 2020");
                                Console.WriteLine($"The answer is {parsedInputs[j] * parsedInputs[i] * parsedInputs[k]}\nThis took {comparisons} comparisons.");
                                return;
                            }

                        }
                    }
                    else // Part 1
                    {
                        comparisons++;
                        if (parsedInputs[j] == parsedInputs[i]) continue;
                        if (parsedInputs[j] + parsedInputs[i] == 2020)
                        {
                            Console.WriteLine($"{parsedInputs[j]} (Item #{j+1}) + {parsedInputs[i]} (Item #{i+1}) = 2020");
                            Console.WriteLine($"The answer is {parsedInputs[j] * parsedInputs[i]}\nThis took {comparisons} comparisons.");
                            return;
                        }
                    }
                }
            }
        }
    }
}
