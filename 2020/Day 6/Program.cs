﻿using System;
using System.Collections.Generic;
using AdventOfCode.Utilities;

namespace AdventOfCode.Year2020.Day6
{
    class Program
    {
        public static int ParseLineToBits(string inputLine)
        {
            // Ignores last 6 bits. Bits are lined up with a at 0, through z at 25. Bit shifting based on ascii value minus base (a)
            int result = 0;
            foreach (var character in inputLine)
            {
                result |=  0x1 << character - 'a';
            }

            return result;
        }
        
        static void Main(string[] args)
        {
            string[] inputs = InputGetter.GetInputs(args);
            
            int firstLineResult = ParseLineToBits(inputs[0]); // Expect this to return 1585182
            Console.WriteLine($"{firstLineResult}");

            int countedBits = BitOps.CountBits(firstLineResult); // expect this to be 8
            Console.WriteLine($"{countedBits}");


            List<Group> Groups = new List<Group>();
            List<string> CurrentGroup = new List<string>();
            for (int i = 0; i < inputs.Length; i++)
            {
                if (inputs[i] == "")
                {
                    Groups.Add(new Group(CurrentGroup));
                    CurrentGroup = new List<string>();
                }
                else
                {
                    CurrentGroup.Add(inputs[i]);
                }
            }
            Groups.Add(new Group(CurrentGroup));

            int sum = 0;
            int andSum = 0;
            foreach (var group in Groups)
            {
                sum += group.Bits1;
                andSum += group.Bits2;
            }

            Console.WriteLine($"The sum of the answered questions is {sum}");
            Console.WriteLine($"The number of questions that everyone abnswered yes to is {andSum}");

        }


    }
    class Group
    {
        public int GroupAnswers1;
        public int GroupAnswers2 = Int32.MaxValue;
        public List<string> Answers;
        public int Bits1;
        public int Bits2;
        public Group(List<string> answers)
        {
            this.Answers = answers;
            this.parseAnswersToInt();
            this.Bits1 = BitOps.CountBits(GroupAnswers1);
            this.Bits2 = BitOps.CountBits(GroupAnswers2);
        }
        private void parseAnswersToInt()
        {
            foreach (var answer in this.Answers)
            {
                this.GroupAnswers1 |= Day6.Program.ParseLineToBits(answer);
                this.GroupAnswers2 &= Day6.Program.ParseLineToBits(answer);
            }
        }
    }
}
