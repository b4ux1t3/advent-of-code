﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using AdventOfCode.Utilities;

namespace AdventOfCode.Year2020.Day2
{
    class Program
    {
        public class InputLine
        {
            public int min { get; private set; }
            public int max { get; private set; }
            public char targetLetter { get; private set; }
            public string password { get; private set; }

            public InputLine(int min, int max, char target, string pass)
            {
                this.min = min; 
                this.max = max;
                this.targetLetter = target;
                this.password = pass;
            }
        }

        static InputLine ParseLine(string line)
        {
            string pattern = "(?<minimum>\\d+)\\-(?<maximum>\\d+)\\ (?<character>\\w):\\ (?<password>\\w+)";
            var matches = Regex.Matches(line, pattern);
            var min = Convert.ToInt32(matches[0].Groups["minimum"].Value);
            var max = Convert.ToInt32(matches[0].Groups["maximum"].Value);
            var targetLetter = Convert.ToChar(matches[0].Groups["character"].Value);
            string password = matches[0].Groups["password"].Value;
            InputLine newLine = new InputLine(min, max, targetLetter, password);

            return newLine;
        }

        static bool IsValidPart1(InputLine line)
        {
            int count = 0;
            char currentChar = line.targetLetter;
            foreach (var letter in line.password)
            {

                if (Convert.ToChar(letter) == currentChar) count++;
            }

            return count <= line.max && count >= line.min;
        }

        static bool IsValidPart2(InputLine line)
        {
            bool isOnMin = line.password[line.min-1] == line.targetLetter;
            bool isOnMax = line.password[line.max-1] == line.targetLetter;

            return isOnMin ^ isOnMax;
        }

        static void Main(string[] args)
        {
            var inputs = InputGetter.GetInputs(args);
            
            List<InputLine> lines = new List<InputLine>();

            foreach (var line in inputs)
            {
                lines.Add(ParseLine(line));
            }

            Console.WriteLine($"There are {lines.ToArray().Length} passwords.");
            int goodPasswords = 0;
            if (args.Length > 1 && args[1] == "true")
            {
                foreach (var line in lines)
                {
                    if (IsValidPart2(line)) goodPasswords++;
                }
            }
            else
            {
                foreach (var line in lines)
                {
                    if (IsValidPart1(line)) goodPasswords++;
                }
            }
            Console.WriteLine($"There are {goodPasswords} valid passwords.");
        }
    }
}
