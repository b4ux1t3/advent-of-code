﻿using System;
using System.Collections.Generic;
using System.Linq;
using AdventOfCode.Utilities;

namespace AdventOfCode.Year2020.Day9
{
    public static class Program
    {
        static void Main(string[] args)
        {
            var inputs = InputGetter.GetInputs(args).ToList();
            var inputInts = inputs.Select(i => UInt64.Parse(i)).ToList();

            var i = 25;
            var match = true;
            while (match )
            {
                match = MatchesWindow(inputInts.ToArray(), ++i, 25);
            }
            Console.WriteLine($"First Non-Matching Value: {inputInts[i]}");
        }

        static bool MatchesWindow(ReadOnlySpan<ulong> ints, int index, int windowSize = 25)
        {
            var sums = SumAllPairs(ints.Slice(index-windowSize-1, windowSize+1));
            foreach (var t in sums)
            {
                if (ints[index] == t)
                {
                    return true;
                }
            }
            return false;
        }

        static List<ulong> SumAllPairs(ReadOnlySpan<ulong> sumables)
        {
            var returnList = new List<ulong>();
            for (var i = 0; i < sumables.Length; i++)
            {
                for (var j = 0; j < sumables.Length; j++)
                {
                    if (sumables[j]!=sumables[i]) returnList.Add(sumables[i]+sumables[j]);
                }
            }

            return returnList;
        } 

    }
}
