﻿module AdventOfCode.Year2023.DayOne.PartOne

let getNumbersPartOne (str: string) =
    let digits =
        str.ToCharArray()
        |> Array.filter System.Char.IsDigit
        |> Array.toList
    match digits with
    | [ d ] -> $"%c{d}%c{d}"
    | [ d1; d2 ] -> $"%c{d1}%c{d2}"
    | head::tail -> $"%c{head}%c{List.last tail}"
    | _ -> "INVALID"