﻿module AdventOfCode.Year2023.DayOne.PartTwo

open System.Text.RegularExpressions

let validStrings =
    [|
        "one" ;
        "two" ;
        "three" ;
        "four" ;
        "five" ;
        "six" ;
        "seven" ;
        "eight" ;
        "nine"
    |]

let tryMatchOne str =
    match str with
    | "one" -> Some "1"
    | _ -> None

let tryMatchTwo str =
    match str with
    | "two" -> Some "2"
    | _ -> None

let tryMatchThree str =
    match str with
    | "three" -> Some "3"
    | _ -> None

let tryMatchFour str =
    match str with
    | "four" -> Some "4"
    | _ -> None

let tryMatchFive str =
    match str with
    | "five" -> Some "5"
    | _ -> None

let tryMatchSix str =
    match str with
    | "six" -> Some "6"
    | _ -> None

let tryMatchSeven str =
    match str with
    | "seven" -> Some "7"
    | _ -> None

let tryMatchEight str =
    match str with
    | "eight" -> Some "8"
    | _ -> None

let tryMatchNine str =
    match str with
    | "nine" -> Some "9"
    | _ -> None

let matchesValue (str: string) index numbers =
    let result =
        match str[index] with
        | '1' -> Some "1"
        | '2' -> Some "2"
        | '3' -> Some "3"
        | '4' -> Some "4"
        | '5' -> Some "5"
        | '6' -> Some "6"
        | '7' -> Some "7"
        | '8' -> Some "8"
        | '9' -> Some "9"
        | 'o' ->
            match index+2 < str.Length with
            | true -> str.Substring(index, 3) |> tryMatchOne
            | false -> None
        | 't' ->
            match str[index+1] with
            | 'w' ->
                match index+2 < str.Length with
                | true -> str.Substring(index, 3) |> tryMatchTwo
                | false -> None
            | 'h' ->
                match index+4 < str.Length with
                | true -> str.Substring(index, 5) |> tryMatchThree
                | false -> None
            | _ -> None
        | 'f' ->
            match str[index+1] with
            | 'o' ->
                match index+3 < str.Length with
                | true -> str.Substring(index, 4) |> tryMatchFour
                | false -> None
            | 'i' ->
                match index+3 < str.Length with
                | true -> str.Substring(index, 4) |> tryMatchFive
                | false -> None
            | _ -> None
        | 's' ->
            match str[index+1] with
            | 'i' ->
                match index+2 < str.Length with
                | true -> str.Substring(index, 3) |> tryMatchSix
                | false -> None
            | 'e' ->
                match index+4 < str.Length with
                | true -> str.Substring(index, 5) |> tryMatchSeven
                | false -> None
            | _ -> None
        | 'e' ->
            match index+4 < str.Length with
            | true -> str.Substring(index, 5) |> tryMatchEight
            | false -> None
        | 'n' ->
            match index+3 < str.Length with
            | true -> str.Substring(index, 4) |> tryMatchNine
            | false -> None
        | _ -> None
        
    match result with
    | Some number -> index+1, str, (Seq.append numbers [number])
    | None -> index+1, str, numbers

let rec solveString index numbers (str: string) =
    match index = str.Length - 1 with
    | true -> numbers // Default case; break from the recursion
    | false ->    // Do some crunching.
        match matchesValue str index numbers with
        | index, newStr, numbers -> solveString index numbers newStr
    