﻿module AdventOfCode.Year2023.DayOne.PartTwoCorrect
open System.Text.RegularExpressions
open System.Linq

let convertStringToDigit str =
    match str with
    | "one" -> "1" 
    | "two" -> "2" 
    | "three" -> "3" 
    | "four" -> "4" 
    | "five" -> "5" 
    | "six" -> "6" 
    | "seven" -> "7" 
    | "eight" -> "8" 
    | "nine" -> "9"
    | "1" 
    | "2" 
    | "3" 
    | "4" 
    | "5" 
    | "6" 
    | "7" 
    | "8" 
    | "9" -> str
    | _ -> failwith "You shouldn't have gotten here."

let getNumbers (str: string) =
    let nums =
        Regex
            .Matches(str, "\d|one|two|three|four|five|six|seven|eight|nine")
            .ToArray()
        |> Array.map (fun m -> m.Value)
        |> Array.map convertStringToDigit
    System.String.Join("", nums)
    // let joined = System.String.Join("", nums)
    // PartOne.getNumbersPartOne joined
    
        
    
    