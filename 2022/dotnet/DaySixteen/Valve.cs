﻿namespace DaySixteen;

public record Valve
{
    public string Name { get; init; }
    
    public int FlowRate { get; init; }
    
    public string[] ValveNames { get; init; }

    public List<Valve> LeadsTo { get; } = new();

    public Valve(string name, int flowRate, string[] valveNames)
    {
        Name = name;
        FlowRate = flowRate;
        ValveNames = valveNames;
    }
}