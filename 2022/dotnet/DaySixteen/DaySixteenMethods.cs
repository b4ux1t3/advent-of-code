﻿using System.Text.RegularExpressions;

namespace DaySixteen;

public static class DaySixteenMethods
{
    private const string ValveRegex =
        @"Valve (?<valveName>[A-Z]{2}) has flow rate=(?<flowRate>\d+); tunnel(?:s)? lead(?:s)? to valve(?:s)? (?<leadsTo>([A-Z]{2}(?:, )?)+)";
    public static Valve ParseLine(this string line)
    {
        var match = Regex.Match(line, ValveRegex);
        var name = match.Groups["valveName"].Value;
        var flowRate = Convert.ToInt32(match.Groups["flowRate"].Value);
        var leadsTo = Regex.Split(match.Groups["leadsTo"].Value, ", ");

        return new(name, flowRate, leadsTo);
    } 
}
