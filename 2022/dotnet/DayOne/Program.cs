﻿using System.Text.RegularExpressions;
using AdventOfCode.Utilities;

var input = InputGetter.GetInputs(args) ?? throw new InvalidOperationException("The input file needs to have input!");

var elves = new List<long>();
var currentElf = new List<long>();

for (var i = 0; i < input.Length; i++)
{
    var match = Regex.Match(input[i], @"\d+");
    if (match.Success)
    {
        currentElf.Add(Convert.ToInt64(input[i]));
    }
    else
    {
        elves.Add(currentElf.Sum());
        currentElf.Clear();
    }
}

elves.Sort();
var answerPartOne = elves.Max();
var answerPartTwo = elves.Skip(elves.Count - 3).Take(3).Sum();

Console.WriteLine($"Part One: {answerPartOne}");
Console.WriteLine($"Part Two: {answerPartTwo}");
