﻿namespace DayFour;

public static class DayFourMethods
{
    // Part One
    public static bool TestForContainment(this ((int, int), (int, int)) line, bool partTwo = false)
    {
        var elfOne = line.Item1;
        var elfTwo = line.Item2;
        return !partTwo
            ? (elfOne.Item1 >= elfTwo.Item1 && elfOne.Item2 <= elfTwo.Item2) ||
              (elfTwo.Item1 >= elfOne.Item1 && elfTwo.Item2 <= elfOne.Item2)
            : IsIntInRange(elfOne.Item1, elfTwo) ||
              IsIntInRange(elfOne.Item2, elfTwo) ||
              IsIntInRange(elfTwo.Item1, elfOne) ||
              IsIntInRange(elfTwo.Item2, elfOne);

    }

    public static (string, string) SplitLine(this string line)
    {
        var split = line.Split(',');
        return (split[0], split[1]);
    }

    public static ((int, int), (int, int)) ParseLine(this (string, string) line)
    {
        return
        (
            ParseInts(line.Item1),
            ParseInts(line.Item2)
        );
    }
    
    private static (int, int) ParseInts(this string str)
    {
        var split = str.Split('-');
        return (Convert.ToInt32(split[0]), Convert.ToInt32(split[1]));
    }

    private static bool IsIntInRange(int testSubject, (int, int) range) =>
        testSubject >= range.Item1 && testSubject <= range.Item2;
}