﻿// See https://aka.ms/new-console-template for more information

using System.Net;
using AdventOfCode.Utilities;
using DayFour;

var inputs = InputGetter.GetInputs(args) ?? throw new InvalidOperationException("Need inputs, kthxbai.");

var partOneContainmentChecks = new bool[inputs.Length];
var partTwoContainmentChecks = new bool[inputs.Length];

Parallel.ForEach(inputs, CheckContainment);

void CheckContainment(string line, ParallelLoopState blah, long i)
{
    var split = line.SplitLine();
    var parsed = split.ParseLine();
    partOneContainmentChecks[i] = parsed.TestForContainment();
    partTwoContainmentChecks[i] = parsed.TestForContainment(true);
}

var partOneAnswer = partOneContainmentChecks.Aggregate(
    0, 
    (state, val) => state += val ? 1 : 0
);

var partTwoAnswer = partTwoContainmentChecks.Aggregate(
    0, 
    (state, val) => state += val ? 1 : 0
);;


Console.WriteLine($"Part One: {partOneAnswer}");
Console.WriteLine($"Part Two: {partTwoAnswer}");