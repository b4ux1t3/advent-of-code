﻿using System.Numerics;

namespace AdventOfCode.Year2022.DayNine;

public static class DayNineMethods
{
    public static (char, int) TokenizeInstruction(this string str)
    {
        var direction = str[0];
        var mag = Convert.ToInt32(str.Substring(2));

        return (direction, mag);
    }

    public static (int, int) ParseInstruction(this (char, int) ins)
    {
        return ins.Item1 switch
        {
            'U' => (0, 1),
            'D' => (0, -1),
            'R' => (1, 0),
            'L' => (-1, 0),
            _ => throw new InvalidDataException("We don't have a definition for that character")
        };
    }

    public static (int, int) Add(this (int, int) a, (int, int) b) => (a.Item1 + b.Item1, a.Item2 + b.Item2);

    public static (int, int) FindTailDestination(this (int, int) tail, (int, int) head)
    {
        var (headX, headY) = head;
        var (tailX, tailY) = tail;

        if (tail == head) return head;
        // Bottom of Head
        if (headY - tailY == 2) return (headX, headY - 1);
        // Top of Head
        if (headY - tailY == -2) return (headX, headY + 1);
        // Left of Head
        if (headX - tailX == 2) return (headX - 1, headY);
        // Right of Head
        if (headX - tailX == -2) return (headX + 1, headY);

        return tail;
    }
}