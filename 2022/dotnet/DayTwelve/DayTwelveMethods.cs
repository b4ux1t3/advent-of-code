﻿namespace DayTwelve;

public static class DayTwelveMethods
{
    public enum Direction
    {
        North,
        South,
        East,
        West
    }
    
    public static int[] GetMapRow(this string str)
    {
        var row = new int[str.Length];
        for (var i = 0; i < row.Length; i++)
        {
            var currentChar = str[i];
            row[i] = currentChar switch
            {
                'S' => 0,
                'E' => 25,
                _ => currentChar - 97
            };
        }

        return row;
    }

    public static (int, int) FindStart(string[] inputs)
    {
        for (var i = 0; i < inputs.Length; i++)
        {
            for (var j = 0; j < inputs[i].Length; j++)
            {
                if (inputs[i][j] == 'S') return (j, i);
            }
        }

        throw new InvalidDataException("Don't have a start point?");
    }
    
    public static (int, int) FindEnd(string[] inputs)
    {
        for (var i = 0; i < inputs.Length; i++)
        {
            for (var j = 0; j < inputs[i].Length; j++)
            {
                if (inputs[i][j] == 'E') return (j, i);
            }
        }

        throw new InvalidDataException("Don't have an end point?");
    }

    public static (int, int)[] GetMovementOptions(this (int, int) location, int[][] map, IReadOnlyList<(int, int)> previousMoves, (int, int) goal)
    {
        var currentValue = map.GetValueAt(location);
        
        var north = (location.Item1, location.Item2 - 1);
        var south = (location.Item1, location.Item2 + 1);
        var east  = (location.Item1 + 1, location.Item2);
        var west  = (location.Item1 - 1, location.Item2);
        
        if (north == goal) return new [] {north};
        if (south == goal) return new [] {south};
        if (east == goal)  return new [] {east};
        if (west == goal)  return new [] {west};
        
        var northValue = north.Item2 >= 0 && north.Item2 < map.Length    ? map.GetValueAt(north) : -1;
        var southValue = south.Item2 >= 0 && south.Item2 < map.Length    ? map.GetValueAt(south) : -1;
        var eastValue  = east.Item1  >= 0 && east.Item1  < map[0].Length ? map.GetValueAt(east)  : -1;
        var westValue  = west.Item1  >= 0 && west.Item1  < map[0].Length ? map.GetValueAt(west)  : -1;

        var destinations = new List<(int, int)>();
        
        if (northValue > -1 && Math.Abs(northValue - currentValue) < 2 && !previousMoves.Contains(north)) destinations.Add(north);
        if (southValue > -1 && Math.Abs(southValue - currentValue) < 2 && !previousMoves.Contains(south)) destinations.Add(south);
        if (eastValue  > -1 && Math.Abs(eastValue  - currentValue) < 2 && !previousMoves.Contains(east))  destinations.Add(east);
        if (westValue  > -1 && Math.Abs(westValue  - currentValue) < 2 && !previousMoves.Contains(west))  destinations.Add(west);
        
        return destinations.ToArray();
    }

    public static (int, int)[] Search((int, int) position, int[][] map, IReadOnlyList<(int, int)> previousMoves, (int, int) goal)
    {
        var moveOptions = position.GetMovementOptions(map, previousMoves, goal);
        if (moveOptions.Length == 0) return Array.Empty<(int, int)>();
        var newPreviousMoves = new List<(int, int)>();
        newPreviousMoves.AddRange(previousMoves);
        newPreviousMoves.Add(position);
        if (moveOptions.Contains(goal))
        {
            newPreviousMoves.Add(goal);
            return newPreviousMoves.ToArray();
        }

        var childrenSearches = moveOptions.Select(option => Search(option, map, newPreviousMoves, goal))
            .Where(answer => answer.Length > 0).ToArray();
        if (childrenSearches.Length == 0) return Array.Empty<(int, int)>();

        return childrenSearches.MinBy(c => c.Length) ?? Array.Empty<(int, int)>();
    }

    private static int GetValueAt(this int[][] arr, (int, int) coord) => arr[coord.Item2][coord.Item1];

    public static Direction GetDirectionOfMove((int, int) from, (int, int) to)
    {
        if (from.Item1 < to.Item1) return Direction.East;
        if (from.Item1 > to.Item1) return Direction.West;
        if (from.Item2 > to.Item2) return Direction.South;
        return Direction.North;
    }

    public static char GetMovementCharacter(this Direction dir)
    {
        return dir switch
        {
            Direction.North => '^',
            Direction.South => 'v',
            Direction.East => '>',
            Direction.West => '<',
            _ => throw new ArgumentOutOfRangeException(nameof(dir), dir, null)
        };
    }
}