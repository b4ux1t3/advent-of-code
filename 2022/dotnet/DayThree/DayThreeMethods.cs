﻿namespace AdventOfCode.Year2022.DayThree;

public static class DayThreeMethods
{
    public static int GetPointValue(this char character) =>
        (int) character switch
        {
            (>= 97) and (< 123) => character - 96,
            (>= 65) and (< 91) => character - 38,
            _ => throw new InvalidDataException($"Received an invalid character: {character}")
        };

    public static (string, string) SplitString(this ReadOnlySpan<char> str)
    {
        if (str.Length % 2 != 0) throw new InvalidDataException("String must be an even length");

        return (
            str[..(str.Length / 2)].ToString(), 
            str[(str.Length / 2)..].ToString()
        );
    }

    public static int GetScore(this (string, string) strTup)
    {
        for (var i = 0; i < strTup.Item1.Length; i++)
        {
            var currentChar = strTup.Item1[i];
            if (!strTup.Item2.Contains(currentChar)) continue;
            return currentChar.GetPointValue();
        }

        return 0;
    }
    
    public static (string, string, string)[] GetGroups(this string[] inputs)
    {
        if (inputs.Length % 3 != 0) throw new InvalidDataException("The inputs should be divisible into groups of three.");
        var groups = new List<(string, string, string)>();
        for (var i = 0; i < inputs.Length; i += 3)
        {
            groups.Add((
                inputs[i],
                inputs[i+1],
                inputs[i+2]
            ));
        }

        return groups.ToArray();
    }

    public static int GetGroupScore(this (string, string, string) strTup)
    {
        var strings = new List<string> {strTup.Item1, strTup.Item2, strTup.Item3};
        strings.Sort((x, y) => x.Length < y.Length ? -1 : x.Length > y.Length ? 1 : 0);
        
        for (var i = 0; i < strings[^1].Length; i++)
        {
            var currentChar = strings[^1][i];
            if (strings[0].Contains(currentChar) && strings[1].Contains(currentChar))
                return currentChar.GetPointValue();
        }

        return 0;
    }
}