﻿using System.Diagnostics;
using AdventOfCode.Utilities;
using AdventOfCode.Year2022.DayThree;

var inputs = InputGetter.GetInputs(args) ?? throw new InvalidOperationException($"Need a file for input plz: {args[0]}");
var sw = new Stopwatch();
sw.Start();

// Part One
var partOneSplit = inputs.Select(str => str.AsSpan().SplitString()).ToArray();
var partOneValues = new int[partOneSplit.Length];
var partOneAnswer = 0;

var partOne =  
    Task.Run(() =>
    {
        Parallel.ForEach(partOneSplit, ParseTuple);
        partOneAnswer = partOneValues.Sum();
        Console.WriteLine($"Part One: {partOneAnswer}");
    });

// PART TWO
var partTwoSplit = inputs.GetGroups();
var partTwoValues = new int[partTwoSplit.Length];
var partTwoAnswer = 0;

var partTwo = 
    Task.Run(() =>
    {
        Parallel.ForEach(partTwoSplit, ParseGroup);
        partTwoAnswer = partTwoValues.Sum();
        Console.WriteLine($"Part Two: {partTwoAnswer}");        
    });
while (!partOne.IsCompleted && !partTwo.IsCompleted) { }
Console.WriteLine($"Finished in: {sw.Elapsed}");


void ParseTuple((string, string) tup, ParallelLoopState blah, long i)
{
    partOneValues[i] = tup.GetScore();
}
void ParseGroup((string, string, string) tup, ParallelLoopState blah, long i)
{
    partTwoValues[i] = tup.GetGroupScore();
}
