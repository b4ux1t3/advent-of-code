﻿namespace AdventOfCode.Year2022.DayEight;

public static class DayEightMethods
{
    public static (bool, int) CheckVisibility(int column, int row, Span<string> grid)
    {
        var sut = grid[row][column];

        bool canSeeFromTop = true;
        int treesNorth = 0;
        if (row != 0)
        {
            for (var i = row - 1; i >= 0; i--)
            {
                var comparison = grid[i][column];
                treesNorth++;
                if (comparison >= sut)
                {
                    canSeeFromTop = false;
                    break;
                }
            }
        }
        
        bool canSeeFromBottom = true;
        int treesSouth = 0;
        if (row != grid.Length)
        {
            for (var i = row + 1; i < grid.Length; i++)
            {
                var comparison = grid[i][column];
                treesSouth++;
                if (comparison >= sut)
                {
                    canSeeFromBottom = false;
                    break;
                }
            }
        }
        
        bool canSeeFromLeft = true;
        int treesWest = 0;
        if (column != 0)
        {
            for (var i = column - 1; i >= 0; i--)
            {
                var comparison = grid[row][i];
                treesWest++;
                if (comparison >= sut)
                {
                    canSeeFromLeft = false;
                    break;
                }

            }
        }
        bool canSeeFromRight = true;
        int treesEast = 0;
        if (column != grid[0].Length)
        {
            for (var i = column + 1; i < grid[0].Length; i++)
            {
                var comparison = grid[row][i];
                treesEast++;
                if (comparison >= sut)
                {
                    canSeeFromRight = false;
                    break;
                }
            }
        }

        return (
                canSeeFromBottom || canSeeFromLeft || canSeeFromRight || canSeeFromTop, 
                treesEast * treesNorth * treesSouth * treesWest
        );
    }
    
}