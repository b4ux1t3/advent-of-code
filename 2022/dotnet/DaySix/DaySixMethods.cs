﻿using System.Reflection.Metadata.Ecma335;

namespace AdventOfCode.Year2022.DaySix;

public static class DaySixMethods
{
    public static bool CheckForDuplicates(ReadOnlySpan<char> space)
    {
        var test = false;
        for (var i = 0; i < space.Length; i++)
        {
            var currentSut = space[i];
            for (var j = 0; j < space.Length; j++)
            {
                if (i == j) continue;
                test = test || (currentSut == space[j]);
            }
        }

        return test;
    }
}