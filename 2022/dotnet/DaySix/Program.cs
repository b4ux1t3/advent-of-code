﻿// See https://aka.ms/new-console-template for more information

using AdventOfCode.Utilities;
using AdventOfCode.Year2022.DaySix;

var input = InputGetter.DumpInput(args) ?? throw new InvalidOperationException("Pls gib input file.");

var partOneChecks = new bool[input.Length];
var partTwoChecks = new bool[input.Length];

Parallel.ForEach(input, CheckInput);

void CheckInput(char currentChar, ParallelLoopState blah, long i)
{
    partOneChecks[i] = i < 4 || DaySixMethods.CheckForDuplicates(input.Substring((int) i - 3, 4));
    partTwoChecks[i] = i < 14 || DaySixMethods.CheckForDuplicates(input.Substring((int)i - 13, 14));
}

var partOneAnswer = 0;
var partTwoAnswer = 0;

for(var i = 0; i < partTwoChecks.Length; i++)
{
    if (!partOneChecks[i] && partOneAnswer == 0) partOneAnswer = i + 1;
    if (!partTwoChecks[i] && partTwoAnswer == 0) partTwoAnswer = i + 1;
}

Console.WriteLine($"Part One: {partOneAnswer}");
Console.WriteLine($"Part Two: {partTwoAnswer}");
