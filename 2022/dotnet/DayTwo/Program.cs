﻿// See https://aka.ms/new-console-template for more information

using System.Diagnostics;
using AdventOfCode.Utilities;
using AdventOfCode.Year2022.DayTwo;

var inputs = InputGetter.GetInputs(args) ?? throw new InvalidOperationException("Need me an argument, thanks");


// A -> Rock          A = 65 - 64 = 1
// B -> Paper         B = 66 - 64 = 2
// C -> Scissors      C = 67 - 64 = 3
// X -> Rock          X = 88 - 87 = 1
// Y -> Paper         Y = 89 - 87 = 2
// Z -> Scissors      Z = 90 - 87 = 3

// Win  -> 6
// Tie  -> 3
// Loss -> 0

var partOnePoints = 0;
var partTwoPoints = 0;

var winDictionary = new Dictionary<char, char>
{
    {'A', 'Z'},
    {'B', 'X'},
    {'C', 'Y'}
};

var loseDictionary = new Dictionary<char, char>
{
    {'Z', 'A'},
    {'X', 'B'},
    {'Y', 'C'}
};


var resultDictionary = new Dictionary<char, GameResult>
{
    {'X', GameResult.Lose},
    {'Y', GameResult.Draw},
    {'Z', GameResult.Win},
};
var sw = new Stopwatch();
sw.Start();
for (var i = 0; i < inputs.Length; i++)
{
    // Part One
    var them = inputs[i][0];
    var us = inputs[i][^1];
    var tie = winDictionary[them] != us && winDictionary[(char)(us - 23)] != (char)(them + 23);
    if (tie)
    {
        partOnePoints += us - 87 + 3;
        continue;
    }

    var win = winDictionary[them] != us;

    if (win)
    {
        partOnePoints += us - 87 + 6;
        continue;
    }

    partOnePoints += us - 87;
    
    partTwoPoints += resultDictionary[us] switch
    {
        GameResult.Lose => winDictionary[them] - 23 - 64,
        GameResult.Draw => them - 64 + 3,
        GameResult.Win => loseDictionary[(char) (them + 23)] - 64 + 6,
        _ => throw new ArgumentOutOfRangeException()
    };
}

Console.WriteLine($"Part One: {partOnePoints}");
Console.WriteLine($"Part Two: {partTwoPoints}");
Console.WriteLine($"Finished in: {sw.Elapsed}");

