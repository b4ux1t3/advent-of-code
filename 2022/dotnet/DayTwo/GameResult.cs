﻿namespace AdventOfCode.Year2022.DayTwo;

    
enum GameResult
{
    Lose = 0,
    Draw = 3,
    Win = 6
}