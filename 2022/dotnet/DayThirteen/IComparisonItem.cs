﻿using System.Text;

namespace DayThirteen;

public enum ComparisonItemType
{
   Number,
   List
}

#pragma warning disable CS0660, CS0661
public class ComparisonItem: IComparable<ComparisonItem>
#pragma warning restore CS0660, CS0661
{

    public int? Value;
    public ComparisonItem[]? Values;
    public readonly ComparisonItemType ItemType;

    public ComparisonItem(int value)
    {
        Value = value;
        ItemType = ComparisonItemType.Number;
    }
    
    public ComparisonItem(ComparisonItem[] values)
    {
        Values = values;
        ItemType = ComparisonItemType.List;
    }

    public static bool operator >  (ComparisonItem a, ComparisonItem b) => a.CompareTo(b) >  0;
    public static bool operator <  (ComparisonItem a, ComparisonItem b) => a.CompareTo(b) <  0;
    public static bool operator >= (ComparisonItem a, ComparisonItem b) => a.CompareTo(b) >= 0;
    public static bool operator <= (ComparisonItem a, ComparisonItem b) => a.CompareTo(b) <= 0;
    public static bool operator == (ComparisonItem a, ComparisonItem b) => a.CompareTo(b) == 0;
    public static bool operator != (ComparisonItem a, ComparisonItem b) => a.CompareTo(b) != 0;
    
    public int CompareTo(ComparisonItem? other)
    {
        if (other is null) return 1;
        var result = other.ItemType switch
        {
            ComparisonItemType.Number => ItemType switch
            {
                ComparisonItemType.Number => Value - other.Value,
                ComparisonItemType.List => _compareOurMembersToTheirNumber(other),
                _ => throw new ArgumentOutOfRangeException()
            },
            ComparisonItemType.List => ItemType switch
            {
                ComparisonItemType.Number => _compareOurNumberToTheirMembers(other),
                ComparisonItemType.List => _compareMembers(other),
                _ => throw new ArgumentOutOfRangeException()
            },
            _ => throw new ArgumentOutOfRangeException()
        } ?? throw new NullReferenceException("Our comparison was null");

        return result;
    }

    private int _compareOurMembersToTheirNumber(ComparisonItem other)
    {
        if (Values!.Length == 0) return -1;
        return Values[0].CompareTo(other);
    }

    private int _compareOurNumberToTheirMembers(ComparisonItem other)
    {
        return other.Values!.Length == 0 ? 1 : CompareTo(other.Values![0]);
    }

    private int _compareMembers(ComparisonItem other)
    {
        if (Values!.Length == other.Values!.Length)
        {
            for (var i = 0; i < Values!.Length; i++)
            {
                var comparison = Values[i].CompareTo(other.Values[i]);
                if (comparison != 0) return comparison;
            }

            return 0;
        }

        if (Values.Length > other.Values.Length)
        {
            for (var i = 0; i < other.Values.Length; i++)
            {
                var comparison = Values[i].CompareTo(other.Values[i]);
                if (comparison != 0) return comparison;
            }

            return 1;
        }

        if (Values.Length < other.Values.Length)
        {
            for (var i = 0; i < Values.Length; i++)
            {
                var comparison = Values[i].CompareTo(other.Values[i]);
                if (comparison != 0) return comparison;
            }
            return -1;
        }

        throw new InvalidOperationException("There is no way we did not match any of the above conditions");
    }

    public override string ToString()
    {
        if (ItemType == ComparisonItemType.Number) return Value.ToString() ?? "<BROKEN>";
        var sb = new StringBuilder();
        sb.Append('[');
        var strings = Values!.Select(v => v.ToString()).ToArray();
        for (var i = 0; i < strings.Length; i++)
        {
            sb.Append(strings[i]);
            if (i < strings.Length - 1) sb.Append(',');
        }
        sb.Append(']');
        return sb.ToString();
    }
}