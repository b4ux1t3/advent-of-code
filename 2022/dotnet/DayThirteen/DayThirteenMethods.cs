﻿using System.Text.RegularExpressions;

namespace DayThirteen;

public static class DayThirteenMethods
{
    public static ComparisonItem ParseLine(this string line)
    {
        var previousItems = new Stack<ComparisonItem>();
        var currentItem = new ComparisonItem(Array.Empty<ComparisonItem>());

        for (int i = 1; i < line.Length; i++)
        {
            var currentChar = line[i];
            if (currentChar == '[')
            {
                previousItems.Push(currentItem);
                currentItem = new(Array.Empty<ComparisonItem>());
                continue;
            }
            
            if (currentChar == ']')
            {
                if (previousItems.TryPeek(out var prevItem))
                {
                    _addComparisonItemToList(currentItem, prevItem);
                    currentItem = previousItems.Pop();
                }
                continue;
            };

            if (currentChar == ',') continue;


            if (currentChar == '1')
            {
                if (line[i + 1] == '0')
                {
                    _addCurrentValue(10, currentItem);
                    i++;
                    continue;
                }

                _addCurrentValue(1, currentItem);
                continue;
            }
            
            if (currentChar == '0')
            {
                _addCurrentValue(0, currentItem);
                continue;
            }
            if (currentChar > 49 && currentChar <= 57)
            {
                _addCurrentValue(currentChar - 48, currentItem);
                continue;
            }

            throw new InvalidOperationException($"Did not recognize token: {currentChar}");
        }

        return currentItem;
    }

    private static void _addCurrentValue(int value, ComparisonItem item)
    {
        switch (item.ItemType)
        {
            case ComparisonItemType.Number:
                item.Value = value;
                break;
            case ComparisonItemType.List:
                item.Values = item.Values!.Select(item => item).Append(new ComparisonItem(value))
                    .ToArray();
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }
    private static void _addComparisonItemToList(ComparisonItem value, ComparisonItem item)
    {
        var newValueList = new ComparisonItem[item.Values.Length + 1];
        item.Values.CopyTo(newValueList, 0);
        newValueList[^1] = value;
        item.Values = newValueList;
    }
}