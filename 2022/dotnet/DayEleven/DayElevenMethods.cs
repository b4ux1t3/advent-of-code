﻿using System.Numerics;
using System.Text.RegularExpressions;

namespace DayEleven;

public static class DayElevenMethods
{
    private const string MonkeyRegex = @"Monkey\s+(?<monkeyId>\d+):\n\s+Starting items:\s+(?<startingItems>((\d+(?:,\s)?)+))\n\s+Operation:\s+new\s+=\s+old\s+(?<operation>[\+\*])\s+(?<operand>old|\d+)\n\s+Test:\s+divisible by (?<testDivisor>\d+)\n\s+If true:\s+throw to monkey\s+(?<trueTargetMonkey>\d+)\n\s+If false:\s+throw to monkey\s+(?<falseTargetMonkey>\d+)";

    public static Monkey[] ParseMonkeys(this string input)
    {
        var matches = Regex.Matches(input, MonkeyRegex, RegexOptions.Multiline);

        var returnList = new Monkey[matches.Count];
        for (var i = 0; i < matches.Count; i++)
        {
            var match = matches[i];
            var monkeyId = Convert.ToInt32(match.Groups["monkeyId"].Value);
            var startingItems = match.Groups["startingItems"].Value.Split(", ").Select(s => BigInteger.Parse(s)).ToArray();
            var operation = match.Groups["operation"].Value switch
            {
                "+" => OperationType.Plus,
                "*" => OperationType.Multiply,
                _ => throw new InvalidDataException("Wrong data!")
            };
            var operandType = match.Groups["operand"].Value switch
            {
                "old" => OperandType.Old,
                _ => OperandType.Value
            };
            var operand = operandType switch
            {
                OperandType.Old => 0UL,
                OperandType.Value => BigInteger.Parse(match.Groups["operand"].Value),
                _ => throw new InvalidOperationException("Yeah, wrong exception for this. Rofl.")
            };
            var testDivisor = BigInteger.Parse(match.Groups["testDivisor"].Value);
            var trueTargetMonkey = Convert.ToInt32(match.Groups["trueTargetMonkey"].Value);
            var falseTargetMonkey = Convert.ToInt32(match.Groups["falseTargetMonkey"].Value);

            returnList[i] = new(
                monkeyId,
                startingItems,
                operation,
                operandType,
                operand,
                testDivisor,
                trueTargetMonkey,
                falseTargetMonkey
            );
        }

        return returnList;
    }
}

public enum OperationType
{
    Plus,
    Multiply
}

public enum OperandType
{
    Old,
    Value
}

public class Monkey
{
    public int Id { get; }
    public Queue<BigInteger> Inventory = new();
    public OperationType Operation;
    public OperandType OperandType;
    public BigInteger WorryOperationOperand { get; }
    public BigInteger TestOperand { get; }
    public int TrueTarget { get; }
    public int FalseTarget { get; }

    public Monkey(
        int id, 
        BigInteger[] startingItems, 
        OperationType operation,  
        OperandType operandType,
        BigInteger worryOperationOperand, 
        BigInteger testOperand,
        int trueTarget, 
        int falseTarget
    )
    {
        Id = id;
        foreach (var item in startingItems)
        {
            Inventory.Enqueue(item);
        }
        Operation = operation;
        OperandType = operandType;
        TrueTarget = trueTarget;
        FalseTarget = falseTarget;
        WorryOperationOperand = worryOperationOperand;
        TestOperand = testOperand;
    }

    public BigInteger SetWorryLevel(BigInteger item, bool partTwo = false)
    {
        var inspectionResult = Operation switch
        {
            OperationType.Plus => item + WorryOperationOperand,
            OperationType.Multiply => OperandType switch
            {
                OperandType.Old => item * item,
                OperandType.Value => item * WorryOperationOperand,
                _ => throw new ArgumentOutOfRangeException()
            },
            _ => throw new ArgumentOutOfRangeException()
        };

        return partTwo ? inspectionResult : inspectionResult / 3;
    }

    public int GetTarget(BigInteger item) => item % TestOperand == 0 ? TrueTarget : FalseTarget;

    public void ThrowToMonkey(Monkey monkey, BigInteger item) => monkey.Inventory.Enqueue(item);

}