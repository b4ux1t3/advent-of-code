﻿using System.Text;

namespace DayTen;

public static class DayTenMethods
{
    public static (int, int) ParseInstruction(this string line)
    {
        var split = line.Split(' ');

        return split[0] switch
        {
            "noop" => (0, 1),
            "addx" => (Convert.ToInt32(split[1]), 2),
            _ => throw new InvalidOperationException("We don't recognize your input, dammit")
        };
    }

    public static (int, int) ExecuteInstruction(this (int, int) instruction, (int, int) currentState) =>
        (currentState.Item1 + instruction.Item1, currentState.Item2 + instruction.Item2);

    public static string RenderScanLine(this int[] line)
    {
        var sb = new StringBuilder();
        for (var i = 0; i < line.Length; i++)
        {
            var currentSpritePos = line[i];
            if (i >= currentSpritePos - 2 && i <= currentSpritePos) sb.Append('#');
            else sb.Append('.');
        }

        return sb.ToString();
    }

}