﻿namespace DaySeven;

public class Tree
{
    public readonly Leaf Root;
    public Tree(Leaf root) => Root = root;
    public Leaf? FindLeaf(Func<Leaf, bool> lookupFunction) => Root.Search(lookupFunction);
    public IEnumerable<Leaf> FindLeaves(Func<Leaf, bool> lookupFunction, HashSet<Leaf> cache, bool includeChildren = false) => Root.FindLeaves(lookupFunction, cache, includeChildren);
}

public enum LeafType
{
    Directory,
    File
}
public class Leaf
{
    public string Name { get; }

    public Leaf? Parent
    {
        get => _parent;
        set => _parent ??= value;
    }

    public int Value => _fileSize > 0 ? _fileSize : _children.Sum(l => l.Value);
    public LeafType Type => _fileSize == 0 ? LeafType.Directory : LeafType.File;

    private Leaf? _parent;
    private readonly List<Leaf> _children = new();
    private readonly int _fileSize = 0;

    public Leaf(string name, int size = 0)
    {
        Name = name;
        _fileSize = size;
    }
    public void AddLeaf(Leaf newLeaf)
    {
        newLeaf.Parent = this;
        _children.Add(newLeaf);
    }

    public Leaf? Search(Func<Leaf, bool> lookupFunction, bool onlyChildren = false, bool onlyDirectories = false, bool onlyFiles = false)
    {
        Leaf? nextLeaf = null;
        for (var i = 0; i < _children.Count; i++)
        {
            nextLeaf = lookupFunction(_children[i]) ? _children[i] : _children[i].Search(lookupFunction, onlyChildren);
            if (nextLeaf?.Type == LeafType.File && onlyDirectories) nextLeaf = null;
            if (nextLeaf?.Type == LeafType.Directory && onlyFiles) nextLeaf = null;
            if (nextLeaf is not null) break;
        }

        return nextLeaf;
    }

    public HashSet<Leaf> FindLeaves(Func<Leaf, bool> lookupFunction, HashSet<Leaf> cache, bool includeChildren = false)
    {
        if (lookupFunction(this)) cache.Add(this);
        if (!includeChildren) return cache;
        foreach (var child in _children)
        {
            foreach (var leaf in child.FindLeaves(lookupFunction, cache, includeChildren))
            {
                cache.Add(leaf);
            }
        }
        
        return cache;
    }

    public override string ToString()
    {
        return Name;
    }
}