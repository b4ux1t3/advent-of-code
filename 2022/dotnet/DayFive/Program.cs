﻿// See https://aka.ms/new-console-template for more information

using System.Text;
using AdventOfCode.Utilities;
using DayFive;

var inputs = InputGetter.GetInputs(args) ?? throw new InvalidOperationException("Need an input file, duh.");

var stacks = new Dictionary<int, Stack<char>>
{
    {1, new ()},
    {2, new ()},
    {3, new ()},
    {4, new ()},
    {5, new ()},
    {6, new ()},
    {7, new ()},
    {8, new ()},
    {9, new ()},
};

var index = 0;
var currentLine = inputs[index++];
while (!currentLine.StartsWith(" 1"))
{
    currentLine.ParseContainerLine(stacks);
    currentLine = inputs[index++];
}

var flippedStacks = new Dictionary<int, Stack<char>>();
var partTwoStacks = new Dictionary<int, Stack<char>>();
for (var i = 1; i <= stacks.Keys.Count; i++)
{
    flippedStacks[i] = new();
    partTwoStacks[i] = new();
    while (stacks[i].TryPop(out var currentChar))
    {
        flippedStacks[i].Push(currentChar);
        partTwoStacks[i].Push(currentChar);
    }
}

index++;
currentLine = inputs[index++];

var partTwoStartIndex = index-1;

while (index <= inputs.Length)
{
    currentLine.ParseMoveInstruction(flippedStacks);
    if (index < inputs.Length) currentLine = inputs[index];
    index++;
}

var partOneSb = new StringBuilder();
for (var i = 1; i <= flippedStacks.Keys.Count; i++)
{
    var hasValue = flippedStacks[i].TryPeek(out var currentChar);
    if (hasValue) partOneSb.Append(currentChar);
}
currentLine = inputs[partTwoStartIndex++];
while (partTwoStartIndex <= inputs.Length)
{
    currentLine.ParseMoveInstruction(partTwoStacks, true);
    if (partTwoStartIndex < inputs.Length) currentLine = inputs[partTwoStartIndex];
    partTwoStartIndex++;
}

var partTwoSb = new StringBuilder();
for (var i = 1; i <= partTwoStacks.Keys.Count; i++)
{
    var hasValue = partTwoStacks[i].TryPeek(out var currentChar);
    if (hasValue) partTwoSb.Append(currentChar);
}

Console.WriteLine($"Part One: {partOneSb}");
Console.WriteLine($"Part Two: {partTwoSb}");
