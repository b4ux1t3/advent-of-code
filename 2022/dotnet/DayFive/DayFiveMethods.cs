﻿using System.Text.RegularExpressions;

namespace DayFive;

public static class DayFiveMethods
{
    private const string InstructionRegex = @"move (\d+) from (\d) to (\d)";
    public static void ParseContainerLine(this string line, Dictionary<int, Stack<char>> stacks)
    {
        var columnCount = 0;
        for (var i = 0; i < line.Length; i += 4)
        {
            var currentChar = line[i + 1];
            columnCount++;

            if (currentChar == ' ') continue;
            stacks[columnCount].Push(currentChar);
        }
    }

    public static void ParseMoveInstruction(this string line, Dictionary<int, Stack<char>> stacks, bool partTwo = false)
    {
        var match = Regex.Match(line, InstructionRegex);

        var count = Convert.ToInt32(match.Groups[1].Value);
        var from = Convert.ToInt32(match.Groups[2].Value);
        var to = Convert.ToInt32(match.Groups[3].Value);
        if (!partTwo)
        {
            for (var i = 0; i < count; i++)
            {
                var hasValue = stacks[from].TryPop(out var currentChar);
                if (hasValue) stacks[to].Push(currentChar);
            }

            return;
        }

        var tempStack = new Stack<char>();
        for (var i = 0; i < count; i++)
        {
            var hasValue = stacks[from].TryPop(out var currentChar);
            if (hasValue) tempStack.Push(currentChar);
        }

        while (tempStack.TryPop(out var currentChar))
        {
            stacks[to].Push(currentChar);
        }
    }
}