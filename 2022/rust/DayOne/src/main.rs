use std::env;
use regex::Regex;

use utilities::get_input_lines;

fn main() {
    let args: Vec<String> = env::args().collect();
    let lines = get_input_lines(&args[1], "\n");
    
    let mut elves: Vec<u64> = Vec::new();
    let mut current_elf: Vec<u64> = Vec::new();

    let number_regex = Regex::new(r"\d+").expect("We failed to build our simple regex!");

    for line in lines {
        let caps = number_regex.captures(&line);
        
        match caps {
            Some(_) => {
                let parsed_number = line.parse::<u64>().expect("Tried to parse a non-number string!");
                current_elf.push(parsed_number);
            }
            None => {
                let mut count = 0;
                for num in &current_elf {
                    count += *num;
                }
                elves.push(count);
                current_elf.clear()
            }
        }
    }

    elves.sort();

    let part_one_answer = elves[elves.len() - 1];

    let top_three = elves.split_at(elves.len() - 3).1;
    
    let mut part_two_answer = 0;
    for elf in top_three {
        part_two_answer += elf
    };

    println!("Part One: {part_one_answer}");
    println!("Part Two: {part_two_answer}");

}
