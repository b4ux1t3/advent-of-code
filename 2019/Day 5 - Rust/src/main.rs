use std::error::Error;
use std::fs::File;
use std::io::prelude::*;
use std::path::Path;
use std::vec::Vec;
use std::env;

fn main(){
    let args: Vec<String> = env::args().collect();
    let input_file_name = args[1].to_string();
    let input = load_input(input_file_name);

    let mut program = Program_State{
        instruction_pointer:        0,
        memory:                     input.clone(),
        exiting:                    false,
        next_instruction_offset:    4,
        last_command:               1337,
    };

    program.execute_command();
    while !program.exiting {
        program.execute_command();
    }

    println!("First memory location: {}", program.memory[0]);
}

// This function loads input from a file, automatically converting the
// input strings into integers and returning a vector of integers 
fn load_input(file_name: String) -> Vec<i64>{
    let path = Path::new(&file_name);

    let mut file = match File::open(&path) {
        // Why yes, this is pulled directly from Rust by Example. How did you know?!
        Err(why) => panic!("Couldn't open {}: {}", path.display(), why.description()),
        Ok(file) => file,
    };

    let mut file_contents = String::new();

    match file.read_to_string(&mut file_contents) {
        Err(why) => panic!("Couldn't read {}: {}", path.display(), why.description()),
        Ok(_) => print!("Loaded input string\n")
    }

    let split_inputs: Vec<&str> = file_contents.split(",").collect();

    let mut program = vec![1i64];

    for command in split_inputs{
        
        let parsed: i64 = match command.parse::<i64>(){
            Err(_) => 0,
            Ok(parsed) => parsed,
        };
        program.push(parsed)
    }
    program.drain(0..1);
    return program;
}
#[allow(non_camel_case_types)]
struct Program_State{
    instruction_pointer:        usize,
    memory:                     Vec<i64>,
    next_instruction_offset:    usize,
    last_command:               i64,
    exiting:                    bool,
}

impl Program_State {
    // This function will take in a program state and then execute the instruction at the
    // instruction pointer in memory. Then increments the instruction_pointer
    // Returns a Program_State.
    fn execute_command(&mut self) -> () {
        let instruction = self.memory[self.instruction_pointer];

        
        self.last_command = instruction;

        let result: i64;
        println!("Executing instruction:\n{},", instruction);
        
        // Process our 
        match instruction{
            // For instruction 1, we need to grab two operands and a destination. 
            // So we need to grab the next three code points (?) from memory,
            // add the first two together, and then store the result in the location
            // indicated by the last one.
            // We can go ahead and hard code the jumps here, since we're more or less
            // defining out instruction set here. 
            1   => {
                let operand_1_location      = self.memory[self.instruction_pointer + 1] as usize;
                let operand_2_location      = self.memory[self.instruction_pointer + 2] as usize;
                let operand_1               = self.memory[operand_1_location];
                let operand_2               = self.memory[operand_2_location];
                let destination             = self.memory[self.instruction_pointer + 3] as usize;

                // Each command is going to change the next instruction offset itself.
                self.next_instruction_offset = 4;
                println!("Adding {} to {}", operand_1, operand_2);
                result = operand_1 + operand_2;
                println!("Writing {} to memory location {}", result, destination);
                self.memory[destination] = result;
            },
            // This is going to be the same as instruction 1, but with multiplication instead.
            // We're going to have to duplicate code, but this is going to save us from doing
            // a lot of allocating in later instructions (specifically ones for this week).
            2   => {
                let operand_1_location      = self.memory[self.instruction_pointer + 1] as usize;
                let operand_2_location      = self.memory[self.instruction_pointer + 2] as usize;
                let operand_1               = self.memory[operand_1_location];
                let operand_2               = self.memory[operand_2_location];
                let destination             = self.memory[self.instruction_pointer + 3] as usize;

                self.next_instruction_offset = 4;
                println!("Multiplying {} with {}", operand_1, operand_2);
                result = operand_1 * operand_2;
                println!("Writing {} to memory location {}", result, destination);
                self.memory[destination] = result;
            },
            // 99 will just set our exiting flag to true, causing the loop to break.
            99  => self.exiting = true,
            _   => panic!("Illegal opcode: {}", instruction)
        };
        
        // Once we know what opcode we used, we can go ahead and add our offset to our instruction pointer
        self.instruction_pointer += self.next_instruction_offset;
    }
}