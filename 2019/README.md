# Advent of Code 2019 - North Pole Roulette

My goal for this year's advent of code is to use a different language every day. However, since I am comfortable with certain languages, and not so much in other languages, I didn't want to make it too easy for myself, where I would only do simple puzzles in languages with which I wasn't familiar, and only do complex puzzles in languages I know very well.

To that end, I made a program to automatically pick me a random language from a list or arbitrary languages I came up with. These languages have no specific significance other than that I came up with them off the top of my head. (I'm pretty proud of that, if I'm being honest.) I cheated a little and used my most familiar language on Day 1, just to keep things interesting.

And so, each day is going to have its own folder, since some languages' tool chains require discrete directories to function properly. Aside from that, there is no specific format to each folder; there may be two separate files for the two parts of each day, or there might be a single file that covers both parts of one day (or even more files if I feel like breaking out functionality due to language capabilities, for example).

I will (try to) keep a running list of which languages I am using each day, including adding the language to the directory name. We'll see if I keep up with that.

## Language Usage

Day 1: Python **

Day 2: Rust *

Day 3: TypeScript

Day 4: C++ *

Day 5: Rust

Day 6: Pascal

Day 7: Rust

Day 8: C#

Day 9: Rust

Day 10: Elm

Day 11: JavaScript (or Rust, depending)

## FAQ

### (a.k.a. Questions I think you might ask.)

**Q: Why didn't you just shuffle the list of languages?**

A: Great question. Moving on.

**Q: Where are all the days?!**

A: I'm going to try to keep days on their own branches while the event is ongoing, and then merge them all at the end. That way I don't accidentally spoil anyone. The only difference here is Day 1, because I needed a template and hadn't thought of using branches for the days yet.

**Q: Wait! Your intcode computer days are all in Rust! Are you a Rustacean?**

A: So, I ended up getting swamped at work (because of course I did), and I realized if I kept getting intcode puzzles, I'd have to not only learn a new language for those days, but *re-implement* my entire intcode computer as well in each of those languages. I just flat out don't have enough time for that. As such, I'm taking the result for those days and just pushing that to the next day. 

Rust just happened to be the language I used for my first intcode puzzle, and so now I will be using it for all of them. This is my first project in Rust. Ever. I'm kind of worried that I ended up doing this in Rust, instead of something I was more familiar with, like C++, C# or Go. Oh well!

I won't end up with as many lanugages under my belt, but at least I'll have a working intcode computer!
