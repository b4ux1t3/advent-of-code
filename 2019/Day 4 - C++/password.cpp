#include <iostream>
#include <string>
#include <vector>

// This is going to increment our password guesses.
std::string increment_string(std::string numeric_string);

// This is going to check the password for validity.
bool is_valid_password(std::string password_string);

// This is going to use the above functions to loop through our input and find all of the matches
std::vector<std::string> find_potential_passwords(std::string minimum, std::string maximum);

int main(int argc, char* argv[]){
    std::string inputs[] = {argv[1], argv[2]};

    std::string low_input;
    std::string high_input;

    if (inputs[0][0] < inputs[1][0]){
        low_input = inputs[0];
        high_input = inputs[1];
    } else {
        low_input = inputs[1];
        high_input = inputs[0];
    }
    std::cout << "Low input:\t" << low_input << std::endl << "High Input:\t" << high_input << std::endl;
    
    // Testing increment_string
    std::string new_input = increment_string(low_input);
    std::cout << "Low input Incremented:\t" << new_input << std::endl;

    new_input = increment_string(new_input);
    std::cout << "New input Incremented:\t" << new_input << std::endl;
    
    // Testing is_valid_password
    std::cout << "123445 should be a valid password:\t" << is_valid_password("123445") << std::endl;
    std::cout << "123456 should not be a valid password:\t" << is_valid_password("123456") << std::endl;
    std::cout << "123443 should not be a valid password:\t" << is_valid_password("123443") << std::endl;

    // Now to find all the valid passwords in teh range we're looking for.
    std::cout << "Checking all potential combinations between " << low_input << " and " << high_input << "." << std::endl;
    std::vector<std::string> potential_passwords = find_potential_passwords(low_input, high_input);
    for (int64_t match = 0; match < potential_passwords.size(); match++){
        std::cout << potential_passwords[match] << std::endl;
    }

    std::cout << "Number of potential passwords: " << potential_passwords.size() << std::endl;
    return 0;
}

std::string increment_string(std::string numeric_string){
    int64_t value = std::stoi(numeric_string);
    return std::to_string(++value);    
}

bool is_valid_password(std::string password_string){
    // Keeps track of whether or not we've found a double character yet
    bool double_character = false;
    
    // Starting with the second character. . .
    for (int8_t character = 1; character < password_string.length(); character++){
        // Compare all characters to teh one before it.
        if (password_string[character] < password_string[character - 1]){
            return false;
        }
        if (password_string[character] == password_string[character - 1]){
            double_character = true;
        }
    }
    // If we have made it this far and not found a double character, then we can just return false, its default state.
    // Otherwise, we return true, and it has been switched to such.
    return double_character;
}

std::vector<std::string> find_potential_passwords(std::string minimum, std::string maximum){
    std::string working_string = minimum;

    std::vector<std::string> valid_passwords;

    while (working_string.compare(maximum) <= 0){
        if (is_valid_password(working_string)){
            valid_passwords.push_back(working_string);
        }

        working_string = increment_string(working_string);
    }

    return valid_passwords;
}

