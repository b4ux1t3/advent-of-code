    #include <iostream>
    #include <string>
    #include <vector>

    // This is going to increment our password guesses.
    std::string increment_string(std::string numeric_string);

    // This is going to check the password for validity.
    bool is_valid_password(std::string password_string);

    // This is going to use the above functions to loop through our input and find all of the matches
    std::vector<std::string> find_potential_passwords(std::string minimum, std::string maximum);

    int main(int argc, char* argv[]){
        std::string inputs[] = {argv[1], argv[2]};

        std::string low_input;
        std::string high_input;

        if (inputs[0][0] < inputs[1][0]){
            low_input = inputs[0];
            high_input = inputs[1];
        } else {
            low_input = inputs[1];
            high_input = inputs[0];
        }
        std::cout << "Low input:\t" << low_input << std::endl << "High Input:\t" << high_input << std::endl;

        // Now to find all the valid passwords in teh range we're looking for.
        std::cout << "Checking all potential combinations between " << low_input << " and " << high_input << "." << std::endl;
        std::vector<std::string> potential_passwords = find_potential_passwords(low_input, high_input);
        for (int64_t match = 0; match < potential_passwords.size(); match++){
            std::cout << potential_passwords[match] << std::endl;
        }

        std::cout << "Number of potential passwords: " << potential_passwords.size() << std::endl;
        return 0;
    }

    std::string increment_string(std::string numeric_string){
        int64_t value = std::stoi(numeric_string);
        return std::to_string(++value);    
    }

    bool is_valid_password(std::string password_string){
        // Keeps track of whether or not we've found a double character yet
        bool in_double = false;
        int64_t num_doubles = 0;
        
        // Starting with the second character. . .
        for (int8_t character = 1; character < password_string.length(); character++){
            // Compare all characters to the one before it.
            if (password_string[character] < password_string[character - 1]){
                return false;
            }
            if (password_string[character] == password_string[character - 1]){
                
                if (!in_double){
                    in_double = true;
                    num_doubles++;
                } else {
                     in_double = false;
                     num_doubles--;
                }
            } else {
                in_double = false;
            }
        }
        // If num_doubles here is exactly 1, we should have a proper match.
        return num_doubles == 1;
    }

    std::vector<std::string> find_potential_passwords(std::string minimum, std::string maximum){
        std::string working_string = minimum;

        std::vector<std::string> valid_passwords;

        while (working_string.compare(maximum) <= 0){
            if (is_valid_password(working_string)){
                valid_passwords.push_back(working_string);
            }

            working_string = increment_string(working_string);
        }

        return valid_passwords;
    }

