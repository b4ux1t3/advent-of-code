import random
languages = []

with open("languages.txt", "r") as language_input:
    languages = [language.strip() for language in language_input]

todays_language = languages.pop(random.randrange(len(languages) - 1))
print(f"You're using {todays_language} today.")

with open("used_languages.txt", "r+") as used_languages:
    lines = len([line.strip() for line in used_languages])

    day = lines + 1

    used_languages.write(f"\nday {day}: {todays_language}")

with open("languages.txt", "w") as language_input:
    for language in languages:
        if language != "":
            language_input.write(f"{language}\n")
