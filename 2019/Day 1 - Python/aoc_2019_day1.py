import sys
import math

file_path = sys.argv[1]
modules = []
with open(file_path, "r") as input_file:
    modules = [int(line) for line in input_file]

# Just returns the amount opf fuel for a given amount of mass
def get_fuel(mass):
    return (mass // 3) - 2

# Uses get_fuel to get the total fuel requirement for a module.
def get_fuel_for_module(module_mass):
    fuel_required = 0
    mass_remaining = module_mass

    while mass_remaining > 0:
        new_fuel = get_fuel(mass_remaining)
        # Need an exit condition, or we're going to start subtracting.
        # This is not an infinite loop! It's just going to give us 
        # the wrong answer.
        if new_fuel > 0:
            fuel_required += new_fuel

        mass_remaining = new_fuel
    return fuel_required

outputs = [get_fuel_for_module(module) for module in modules]

print(f"Need {sum(outputs)} fuel for {len(outputs)} modules.")