/// <reference path="node_modules/@types/node/ts3.2/fs.d.ts" />

import * as fs from "fs";
class Instruction{
    direction:  string;
    distance:   number;
    constructor(direction: string, distance: number){
        this.direction  = direction;
        this.distance   = distance
    }
}

class Point{
    x: number;
    y: number;
    manhattanDistance: number;

    calcManhattanDistance(){
        this.manhattanDistance = Math.abs(this.x) + Math.abs(this.y);
    }

    constructor(x: number, y: number){
        this.x = x;
        this.y = y;

        this.calcManhattanDistance();
    }

}

class Line{
    start: Point;
    end: Point;
    orientation: string;
    points: Point[];

    constructor(start: Point, end: Point){
        this.start = new Point(start.x, start.y);
        this.end = new Point(end.x, end.y);
        this.points = [];
        if (this.start.x === this.end.x){
            this.orientation = "VER"
        } else {
            this.orientation = "HOR"
        }

        if (this.orientation == "VER"){
            for (let i = 0; i < Math.max(this.start.y, this.end.y) - Math.min(this.start.y, this.end.y); i++){
                this.points.push(new Point(this.start.x, this.start.y + i))
            }
        } else {
            for (let i = 0; i < Math.max(this.start.x, this.end.x) - Math.min(this.start.x, this.end.x); i++){
                this.points.push(new Point(this.start.x + i, this.start.y))
            }
        }
    }
}

class wirePath{
    position: Point;
    lines: Line[];
    
    path: Instruction[];

    loadNewPath(pathInput: string){
            let steps_strings = pathInput.split(',');
            steps_strings.forEach(step_string => {
                let new_direction = step_string[0];
                let new_distance = step_string.substring(1, step_string.length);
                
                this.path.push(
                    new Instruction(
                        new_direction,
                        Number.parseInt(new_distance) 
                    )
                );
            });
    }

    step(instruction: Instruction){
        let old_pos = new Point(this.position.x, this.position.y);

        // console.log("Moving " + instruction.direction + instruction.distance + " from (" + this.position.x + "," + this.position.y + ")")

        if (instruction.direction == 'U'){
            this.position.y += instruction.distance;
        } else if (instruction.direction == 'D'){
            this.position.y -= instruction.distance;
        } else if (instruction.direction == 'L'){
            this.position.x -= instruction.distance;
        } else if (instruction.direction == 'R'){
            this.position.x += instruction.distance;
        }

        // console.log("New position: (" + this.position.x + "," + this.position.y + ")")

        this.lines.push(new Line(old_pos, this.position));
    }

    constructor(pathInput: string) {
        this.path = [];
        this.loadNewPath(pathInput);
        this.position = new Point(0,0);
        this.lines = [];
        this.path.forEach(instruction => {
            this.step(instruction);
        });
    }

    checkForIntersection(otherPath: wirePath){
        let matches = [];

        // . . .Oh god
        this.lines.forEach(thisLine => { // For each of my lines. . .
            otherPath.lines.forEach(line => { // Check each line in the other path. . .
                thisLine.points.forEach(thisPoint => { // for each of my points in each of my lines. . .
                    line.points.forEach(point => { // for each of the points in each of the other lines. . .
                        if (thisPoint.x === point.x && thisPoint.y === point.y && thisPoint.manhattanDistance != 0){ // If they have the same x and y, and they're not at 0,0, add them to matches.
                            matches.push(point);
                        }
                    });
                });
            });
        });
        
        return matches; 
    }

}

const input_file = fs.readFileSync('/home/alex/Documents/advent-of-code/2019/Day 3 - TypeScript/day3_inputs.txt', 'utf-8');

let inputs = input_file.split('\n');

let path1 = new wirePath(inputs[0]);
let path2 = new wirePath(inputs[1]);

let matches = path1.checkForIntersection(path2);

let shortestDistance = -1

matches.forEach(match => {
    if (match.manhattanDistance < shortestDistance || shortestDistance < 0){
        shortestDistance = match.manhattanDistance;
    }
});
console.log(matches);
console.log(shortestDistance);