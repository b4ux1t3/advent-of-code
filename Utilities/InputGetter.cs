﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading;

namespace AdventOfCode.Utilities
{
    public static class InputGetter
    {
        public static char[] defaultDelim = {'\n'};

        public static string DumpInput(string[] args, int argNumber = 0)
        {
            FileStream inFile;

            try
            {
                if (args.Length >= argNumber)
                {
                    inFile = File.OpenRead(args[argNumber]);
                }
                else
                {
                    inFile = File.OpenRead(Path.Combine(Directory.GetCurrentDirectory(), "input.txt"));
                }
            }
            catch (FileNotFoundException ex)
            {
                Console.Error.WriteLine(ex.Message);
                Environment.ExitCode = -1;
                return null;
            }

            using StreamReader reader = new StreamReader(inFile);
            return reader.ReadToEnd();
        }
        
        public static string[] GetInputs(string[] args, int argNumber = 0) => GetInputs(args, "\n", argNumber);
        public static string[] GetInputs(string[] args, ReadOnlySpan<char> delimExpr, int argNumber = 0)
        {
            FileStream inFile;

            try
            {
                if (args.Length >= argNumber)
                {
                    inFile = File.OpenRead(args[argNumber]);
                }
                else
                {
                    inFile = File.OpenRead(Path.Combine(Directory.GetCurrentDirectory(), "input.txt"));
                }
            }
            catch (FileNotFoundException ex)
            {
                Console.Error.WriteLine(ex.Message);
                Environment.ExitCode = -1;
                return null;
            }
            using (StreamReader reader = new StreamReader(inFile))
            {
                if (delimExpr == defaultDelim || delimExpr == null)
                {
                    List<String> returnList = new List<string>();
                    while (reader.Peek() >= 0)
                    {
                        returnList.Add(reader.ReadLine());
                    }
                    return returnList.ToArray();
                }

                return Regex.Split(reader.ReadToEnd(), delimExpr.ToString());
            }
        }
    }
}
