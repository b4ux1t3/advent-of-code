namespace AdventOfCode.Utilities
{
    public static class BitOps
    {
        public static int CountBits(int number)
        {
            int result = 0;
            for (int i = 0; i < 32; i++)
            {
                result += 0x1 & number >> i;
            }
            return result;
        }
    }
}