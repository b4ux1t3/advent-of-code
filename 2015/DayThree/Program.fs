namespace AdventOfCode.Year2015


module DayThree =
    open System
    open System.Collections.Generic
    open AdventOfCode.Utilities
    let inputs =
        InputGetter.GetInputs(Environment.GetCommandLineArgs(), 1, "".ToCharArray()).[0]
        |> List.ofSeq
    let parse input =
        match input with
        | '^' -> 0,1
        | 'v' -> 0,-1
        | '<' -> -1,0
        | '>' -> 1,0
        |_ -> 0,0
    let addPos x y =
        match x,y with
        | (x1,y1), (x2,y2) -> x1+x2,y1+y2
    let foldToList (directions, listOfVisits: List<int*int>) =
        let finalStop =
            listOfVisits.Add (0,0)
            ((0,0), directions)
            ||> List.fold (fun state input ->
                let newState = addPos state (parse input) 
                listOfVisits.Add newState
                newState
                )
        finalStop
        
    module PartOne =
        let visits = List<int*int>()
        let finalDestination = foldToList (inputs, visits)
        let answer =
            let ans =
                visits
                |> List.ofSeq
                |> List.distinct
            ans.Length
            
        printfn $"Part One: %d{answer}"
    
    module PartTwo =
        let santaVisits = List<int*int>()
        let roboVisits = List<int*int>()
        
        let foldToTwoLists (directions, (listOfVisits1: List<int*int>,listOfVisits2: List<int*int>)) =
            let finalStop =
                listOfVisits1.Add (0,0)
                let mutable i = -1
                ((0,0),directions)
                ||> List.fold (fun state input ->
                    i <- i + 1
                    if i % 2 = 0 then
                        let newState = addPos state (parse input)
                        listOfVisits1.Add newState
                        newState
                    else
                        state
                )
            let finalStop2 =
                listOfVisits2.Add (0,0)
                let mutable i = -1
                ((0,0),directions)
                ||> List.fold (fun state input ->
                    i <- i + 1
                    if i % 2 = 1 then
                        let newState = addPos state (parse input)
                        listOfVisits2.Add newState
                        newState
                    else
                        state
                )
            
            let allStops =
                let one = listOfVisits1 |> List.ofSeq
                let two = listOfVisits2 |> List.ofSeq
                
                (one,two) ||> List.append
                
            allStops
            
        let allStops = foldToTwoLists (inputs, (santaVisits, roboVisits))
        let answer =
            let ans = allStops |> List.distinct
            ans.Length

        printfn $"Part Two: %d{answer}"
       