namespace AdventOfCode.Year2015
module DayTwo =
    open System
    open AdventOfCode.Utilities
    let inputs = InputGetter.GetInputs(Environment.GetCommandLineArgs(), 1) |> List.ofSeq
    let add x y = x + y
    let addTuples x y =
        match x, y with
        | (a, b, c, d), (e, f, g, h) -> (a+e, b+f, c+g, d+h)
    let foldTuple tuple =
        match tuple with
        | (a, b, c, d) -> a + b + c + d
    let parseInts (line: string) =
        let parsedInts =
            line.Split('x')
            |> List.ofSeq
            |> List.map int
        match parsedInts with
        | [x; y; z] -> (x,y,z)
        | _ -> (0,0,0)
    let calcSurfaceArea (x,y,z) = (x*y, x*z, y*z)
    let calcSlop surfaceAreas =
        match surfaceAreas with
        | x, y, z -> ((min (min x y) z), x*2,y*2,z*2)
    let calcRibbon measurements =
        match measurements with
        | x, y, z ->
            if x = y && y = z then
                2*x + 2*y + x*x*x
            else
                let maximum = max (max x y) z
                let others = [x;y;z] |> List.filter (fun num -> num <> maximum)
                if others.Length < 2 then
                    others.[0]*2+maximum*2+x*y*z
                else
                    others.[0]*2+others.[1]*2+x*y*z
    let parsedInputs =
        inputs
        |> List.map parseInts
        
    module PartOne =
        let solution =
            parsedInputs
            |> List.map calcSurfaceArea
            |> List.map calcSlop
            |> List.reduce addTuples
            |> foldTuple
    
        printfn $"Day One: %d{solution}"

    module PartTwo =
        let solution =
            parsedInputs
            |> List.map calcRibbon
            |> List.reduce add

        printfn $"Day Two: %d{solution}"
