module DayOne =
    open System
    open AdventOfCode.Utilities
    let inputs = InputGetter.GetInputs(Environment.GetCommandLineArgs(), 1, "".ToCharArray()).[0]
    let parse position input =
        match input with
        | '(' -> position + 1
        | ')' -> position - 1
        | _   -> position
    let finalPosition =
        (0,inputs) ||> Seq.fold parse
    let firstPositionNegative =
        
        let mutable position = 0
        let mutable i = 0
        let mutable answer = -1 
        inputs |> Seq.iter (fun input ->
            i <- i + 1
            position <- parse position input
            if position < 0 && answer = -1 then answer <- i
            )
        answer
        
    printfn $"Part One: %d{finalPosition}\nPart Two: %d{firstPositionNegative}"