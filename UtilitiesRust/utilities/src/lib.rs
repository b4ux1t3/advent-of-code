use std::fs;

pub fn add(left: usize, right: usize) -> usize {
    left + right
}

pub fn get_input_string(filepath: &String) -> String {
    let contents = fs::read_to_string(filepath).expect("Requires an input file with the puzzle input.");
    contents
}

pub fn get_input_lines(filepath: &String, delim: &str) -> Vec<String> {
    let contents = get_input_string(filepath);
    let split_contents: Vec<String> = contents
                                        .split(delim)
                                        .map(|s| String::from(s))
                                        .collect();
    split_contents
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        let result = add(2, 2);
        assert_eq!(result, 4);
    }
}
